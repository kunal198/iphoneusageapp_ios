//
//  UUColor.m
//  UUChart
//
//  Created by shake on 14-7-24.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUChartConst.h"

@implementation UUColor
+(UIColor *)randomColor
{
    return [self colorWithRed:arc4random()%255/255.0
                        green:arc4random()%255/255.0
                         blue:arc4random()%255/255.0
                        alpha:1.0f];
}

+(UIColor *)randomColorDeep
{
    return [UIColor colorWithRed:(arc4random()%100 + 55)/255.0
                           green:(arc4random()%100 + 55)/255.0
                            blue:(arc4random()%100 + 55)/255.0
                           alpha:1.0f];
}

+(UIColor *)randomColorlight
{
    return [UIColor colorWithRed:(arc4random()%100 + 155)/255.0
                           green:(arc4random()%100 + 155)/255.0
                            blue:(arc4random()%100 + 155)/255.0
                           alpha:1.0f];
}

+(UIColor *)red
{
    return [UIColor colorWithRed:222.0/255.0 green:49.0/255.0 blue:50.0/255.0 alpha:1.0f];
}

+(UIColor *)green
{
    
    return [UIColor colorWithRed:111.0/255.0 green:174.0/255.0 blue:108.0/255.0 alpha:1.0f];
}

+(UIColor *)yellow
{

    return [UIColor colorWithRed:136.0/255.0 green:202.0/255.0 blue:12.0/255.0 alpha:1.0f];
}

+ (CGFloat)getRandomByNum:(int)num {
    
    return (arc4random()%num + 100)/255.0;
}
@end
