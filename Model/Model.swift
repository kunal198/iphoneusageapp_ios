import UIKit
import AVFoundation

class Model: NSObject {
    
    // consumed and total time array
    var consumedTimeArray : NSMutableArray!
    var totalTimeArray :NSMutableArray!
    
    
    // Total Time Bool
    var totalTimeBool = false
    
    // bool used for checking whether app comes from background
    
    var appcomesFromBackground : Bool = false
    
    // parent view Bool
    var passwordUnlock : Bool = false
    var passwordSaved : Bool  = false
    
    // normal alarm variables
    var normalAlarmRingTone   = "AppleUpdate"
    var isVideoalarm : Bool = false
    
    // variable used for saving and retriving data in data base
    var marrStudentData : NSMutableArray!
    var Day: String       = String()
    var Month: String     = String()
    var TotalTime: String = String()
    var Consumed: String  = String()
    var date: String      = String()
    var alarmsaveInDB : Bool = false
    
    // time variable
    var totalTimeforUsage1: String = "00:00"
    var totalTimeForUsage: String  = "00:00"
    var systemSound :Bool          = false
    var recordedSound :Bool        = false
    var mathCapcha :Bool           = false
    var timeBorderViewHidden :Bool = true
    var timerborderViewHeight      = ""
    var firstTime  = ""
    var secondTime = ""
    var thirdTime  = ""
    var firstTone  = ""
    var secondTone = ""
    var thirdTone  = ""
    
    var recordings = [NSURL]()
    var FirstNotification :Bool = true
    var notificationFired :Bool = false
    var firstreminder     :Bool = false
    var secondReminder    :Bool = false
    var thirdReminder     :Bool = false
    var firstReminderTime : NSString = ""
    var secondeminderTime :NSString  = ""
    var thirdReminderTime :NSString  = ""
    var firstReminderRingTone :NSString = ""
    var secondReminderRingTone:NSString = ""
    var thirdReminderRingTone :NSString = ""
    var firstRingToneType     :NSString = ""
    var secondRingToneType    :NSString = ""
    var thirdRinngToneType    :NSString = ""
    var alarmSet :Bool = false
    
    ///Audio player responsible for playing sound files.
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    var starttTime                 = NSTimeInterval(0)
    
    ///User defaults
    var userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    ////// remider variable new
    var timesortedArray : NSMutableArray = NSMutableArray()
    var reminderSet : Bool                                                                                                                  = false

    class func sharedInstance() -> Model {
        return modelSingletonGlobal
    }
}

///Model singleton so that we can refer to this from throughout the app.
let modelSingletonGlobal = Model()