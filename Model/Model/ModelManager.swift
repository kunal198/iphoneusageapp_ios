//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Shikha Kamboj on .
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("iPhoneUsageDB.sqlite"))
        }
        return sharedInstance
    }
    
    func addAlarmInfo(alarmInfo: Model) -> Bool {
        
        print(alarmInfo.TotalTime)
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO DailyiPhoneUsageRecord (Day, Month, TotalTime, Consumed, date) VALUES (?, ?, ?, ?, ?)", withArgumentsInArray: [alarmInfo.Day, alarmInfo.Month, alarmInfo.TotalTime, alarmInfo.Consumed, alarmInfo.date])
        sharedInstance.database!.close()
        return isInserted
    }
   
    func updateAlarmData(alarmInfo: Model) -> Bool {
        
        print(alarmInfo.TotalTime)
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE DailyiPhoneUsageRecord SET Day=?, Month=?, TotalTime=?, Consumed=?, date=? WHERE date=?", withArgumentsInArray: [alarmInfo.Day, alarmInfo.Month, alarmInfo.TotalTime, alarmInfo.Consumed, alarmInfo.date, alarmInfo.date])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func updateConsumedTime(alarmInfo: Model) -> Bool {
        
        print(alarmInfo.Consumed)

        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE DailyiPhoneUsageRecord SET Consumed=? WHERE date=?", withArgumentsInArray: [alarmInfo.Consumed, alarmInfo.date])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func deleteAlarmData(alarmInfo: Model) -> Bool {
        
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM DailyiPhoneUsageRecord WHERE Month=?", withArgumentsInArray: [alarmInfo.Month])
        sharedInstance.database!.close()
        return isDeleted
        
    }
    
    func getAllAlarmData(alarmInfo: Model) -> NSMutableArray {
        
        sharedInstance.database!.open()
        
        print(alarmInfo.Month)
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM DailyiPhoneUsageRecord WHERE Month=?", withArgumentsInArray: [alarmInfo.Month])
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        if (resultSet != nil) {
            
            while resultSet.next() {
                
                let alarmInfo : Model = Model()
                 alarmInfo.Day = resultSet.stringForColumn("Day")
                 alarmInfo.Month = resultSet.stringForColumn("Month")
                 alarmInfo.TotalTime = resultSet.stringForColumn("TotalTime")
                 alarmInfo.Consumed = resultSet.stringForColumn("Consumed")
                 marrStudentInfo.addObject(alarmInfo)
                let str = marrStudentInfo.valueForKey("TotalTime")
                
                print("%@", str)
                print("%@", marrStudentInfo)
                
            }
        }
        sharedInstance.database!.close()
        return marrStudentInfo
    }
}
