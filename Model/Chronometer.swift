//
//  Chronometer.swift
//  Chronometer
//
//  Created by Rafael Veronezi on 8/21/14.
//  Copyright (c) 2014 Syligo. All rights reserved.
//

import Foundation
import UIKit

///
/// Defines a Chronometer class, that can be used to keep track of time.
/// The class uses NSTimer object to keep the refresh pace, which means 
///  that this class is not best swited for precision, since refresh may take
///  longer than specified, based on the available resources.
///
/// This class is not thread safe.
///

public class Chronometer: NSObject {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    //
    // MARK: - Private Properties
    
    //
    // MARK: - Public Properties
    
    public var elapsedTime: NSTimeInterval {

        return appDelegate.elapsedSinceLastRefresh + appDelegate.accumulatedTime
        
    }
    
    /// The Time Interval to refresh the chronometer.The default is 1 second
    public var refreshInterval = NSTimeInterval(0.01)

    /// Determines a time limit for this Chronometer
    public var timeLimit: NSTimeInterval?
    
    /// Optional Block that gets called on each refresh of the specified time interval.
    /// If this class needs to update UI, make sure that the UI class are made in the
    ///  main thread, or dispatched into it.
//    public var updateBlock: ((NSTimeInterval, NSTimeInterval?) -> ())?
    
    /// Optional Block that gets called when the chronometer reach its limit time
    public var completedBlock: (() -> ())?
    
    //
    // MARK: - Initializers
    
    ///
    /// A convenience initializer that allow specifying the refresh interval
    /// - parameter refreshInterval: The desired refresh interval
    ///
    
    public convenience init(refreshInterval: NSTimeInterval) {
        self.init()
        self.refreshInterval = refreshInterval
    }
    
    ///
    /// A convenience initializer that allow specifying the refesh interval and update block
    /// - parameter refreshInterval: The desired refresh interval
    /// - parameter updateBlock: The update block to be called on each refresh
    ///
    
    public convenience init(refreshInterval: NSTimeInterval, updateBlock: (NSTimeInterval, NSTimeInterval?) -> ()) {
        self.init()
        self.refreshInterval = refreshInterval
        appDelegate.updateBlock = updateBlock
    }
    
    //
    // MARK: - Internal Methods
    
    ///
    /// Refresh the timer, calling the update block to notify the tracker about the new value.
    ///
    
    func refreshTime() {
        
        // Calculate the new time
        let refreshTime = NSDate.timeIntervalSinceReferenceDate()
        
        //NSLog("appp start time:-%f", appDelegate.startTime)
        appDelegate.elapsedSinceLastRefresh = (refreshTime - appDelegate.startTime)
        
        // Calculates the remaining time if applicable
        var remainingTime: NSTimeInterval? = nil
        if self.timeLimit != nil {
            remainingTime = self.timeLimit! - elapsedTime
        }

        // If an update block is specified, then call it
        
        appDelegate.updateBlock?(elapsedTime, remainingTime)
        
        // If the chronometer is complete, then call the block and
        if let limit = self.timeLimit {
            if self.elapsedTime >= limit {
                self.stop()
                self.completedBlock?()
            }
        }
    }
    
    //
    // MARK: - Public Methods
    
    ///
    /// Set a time limit for this chronometer
    ///
    public func setLimit(timeLimit: NSTimeInterval, withCompletionBlock completedBlock: () -> ()) {
        self.timeLimit = timeLimit
        self.completedBlock = completedBlock
    }
    
    ///
    /// Start the execution of the Cronometer.
    /// Start will take place using accumulated time from the last session. 
    /// If the Cronometer is running the call will be ignored.
    ///
    
    public func start() {
        
        if !appDelegate.timer.valid {
            // Create a new timer
            
            appDelegate.timer = NSTimer.scheduledTimerWithTimeInterval(self.refreshInterval,
                target: self,
                selector: #selector(Chronometer.refreshTime),
                userInfo: nil,
                repeats: true)

            // Set the base date
            appDelegate.startTime = NSDate.timeIntervalSinceReferenceDate()
           // NSLog("start time%f",appDelegate.startTime)
            
        }
    }
    
    ///
    /// Stops the execution of the Cronometer.
    /// Keeps the accumulated value, in order to allow pausing of the chronometer, and
    ///  to keep "elapsedTime" property value available for the user to keep track.
    ///
    
    public func stop()
    {
        appDelegate.timer.invalidate()
        appDelegate.accumulatedTime = elapsedTime
        NSLog("accumulated time%f",appDelegate.accumulatedTime)
        appDelegate.elapsedSinceLastRefresh = 0
    }
    
    ///
    /// Resets the Cronometer.
    /// This method will stop chronometer if it's running. This is necessary since 
    ///  the class is not thread safe.
    ///
    
    public func reset()
    {
        appDelegate.timer.invalidate()
        appDelegate.elapsedSinceLastRefresh = 0
        appDelegate.accumulatedTime = 0
    }
}
