//
//  ParentControllViewController.swift
//  ScreenUsages
//
//  Created by brst on 8/17/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit


class ParentControllViewController: UIViewController{
    
    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    // following line of code used for making round corner of view
        self.backgroundView.layer.cornerRadius = 10
        self.backgroundView.clipsToBounds = true
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        switchBtn.setOn(false, animated:true)
        
    }
    
    //following button action is used for moving password controll.Like for saving new password or changing existing one
    @IBAction func parentControlBtnActions(sender: AnyObject) {
        if sender.tag == 0 {
            let phoneLockViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("PhoneLockViewController") as!PhoneLockViewController
            self.navigationController?.pushViewController(phoneLockViewcontroller, animated: true)
        }
        else
        {
            if Model.sharedInstance().passwordSaved == true {
                let phoneLockViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("PhoneLockViewController") as!PhoneLockViewController
                self.navigationController?.pushViewController(phoneLockViewcontroller, animated: true)
            }
            else
            {
                let  alert :UIAlertView = UIAlertView()
                alert.title = "Sorry!"
                alert.message = "You didn't set any password"
                alert.addButtonWithTitle("OK")
                alert.show()
            }
        }
    }
    
    //Switch button Action is used for making password on or off
    @IBAction func switchBtnAction(sender: AnyObject) {
        if switchBtn.on {
            
            if Model.sharedInstance().passwordSaved == true {
                let phoneLockViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("PhoneLockViewController") as!PhoneLockViewController
                phoneLockViewcontroller.removePassword = true
                self.navigationController?.pushViewController(phoneLockViewcontroller, animated: true)
            }
            else{
                switchBtn.setOn(false, animated:true)
                let alert : UIAlertView = UIAlertView()
                alert.title = "Sorry!"
                alert.message = "You didn't set any Password"
                alert.addButtonWithTitle("OK")
                alert.show()
            }
        }
    }

    //Following button is used for moving last Screen
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}