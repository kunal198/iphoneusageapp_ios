//
//  AlarmViewController.swift
//  ScreenUsages
//
//  Created by brst on 8/18/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import Foundation
import UIKit

class AlarmViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var alarmButtonClicked  = true
    
    @IBOutlet weak var datePicker_backGroundView: UIView!
    // Model Singleton
    let model = Model.sharedInstance()
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var hour = "00"
    var minute = "00"
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var phoneBackgroundView: UIView!
    @IBOutlet weak var alarmBackgroundView: UIView!
    @IBOutlet weak var timePickerBackgroundView: UIView!
    
    let hours = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
    let minutes = ["0", "1", "2", "3", "4", "5", "6", "7","8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"];
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //make round corner of the views
        self.timePickerBackgroundView.layer.cornerRadius = 10
        self.timePickerBackgroundView.clipsToBounds = true
        
        self.pickerView.layer.cornerRadius = 10
        self.pickerView.clipsToBounds = true
        
        self.alarmBackgroundView.layer.cornerRadius = 10
        self.alarmBackgroundView.clipsToBounds = true
        
        self.phoneBackgroundView.layer.cornerRadius = 10
        self.phoneBackgroundView.clipsToBounds = true
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        self.datePicker_backGroundView.layer.cornerRadius = 10
        self.datePicker_backGroundView.clipsToBounds = true
        
    }
    
    // back button action
    @IBAction func backBtnAction(sender: AnyObject) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(SettingViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    // choose and set alarm
    @IBAction func alarmButtonAction(sender: AnyObject) {
        
        self.datePicker?.hidden = false
        self.phoneBackgroundView?.hidden = true
        self.timePickerBackgroundView?.hidden = false
        self.pickerView?.hidden = true
        alarmButtonClicked = true
        
    }
    
    // phone button action
    @IBAction func phoneButtonAction(sender: AnyObject) {
        
        self.datePicker?.hidden = true
        self.phoneBackgroundView?.hidden = true
        self.timePickerBackgroundView?.hidden = false
        self.pickerView?.hidden = false
        
    }
    
    // pragma MARK :- PickerView DataSource and Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 4
    }
     
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return hours.count
        }
        else if component == 1{
            return 1
        }
        else if component == 2{
            return minutes.count
        }
        else
        {
            return 1
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if component == 0 {
            return hours[row]
        }
        else if component == 1{
           return "hours"
        }
        else if component == 2{
           return minutes[row]
        }
        else{
            return "min"
        }
    }
    
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        
        if component == 0 {
            return 35
        }
        else if component == 1{
            return 70
        }
        else if component == 2{
            return 45
        }
        else
        {
            return 50
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        Model.sharedInstance().alarmSet = true
        
        let obj:Chronometer=Chronometer()
        
        //obj.reset()
        obj.stop()
        
        //Arrays used for showing time on home Screen
        
        let hourarray = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        let minutearray = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"];
        
        if component == 0 {
            hour = hourarray[row]
            
            model.totalTimeForUsage = String(format:"%@:%@",hour,minute)
            NSLog("TotalTime:-%@", model.totalTimeForUsage)
            

        }
        else {
            minute = minutearray[row]
            model.totalTimeForUsage = String(format:"%@:%@",hour,minute)
            NSLog("TotalTime:-%@", model.totalTimeForUsage)
        }
    }
    
    @IBAction func pickerDoneBtnAction(sender: AnyObject) {
        
        NSLog("alarm tone.....%@", Model.sharedInstance().normalAlarmRingTone)
        
        // normal alarm set
        if alarmButtonClicked {
            let date                 = datePicker.date
            
            let dateFormatter        = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let timeStr              = dateFormatter.stringFromDate(date)
            
            let date11               = dateFormatter.dateFromString(timeStr)
            
            let AlarmNotification: UILocalNotification = UILocalNotification()
            AlarmNotification.alertBody                = "Alarm! Alarm! Alarm!"
            AlarmNotification.alertAction              = "Open App"
            AlarmNotification.category                 = "myAlarmCategory"
            
            if Model.sharedInstance().normalAlarmRingTone == "Math Equation" {
                AlarmNotification.soundName                = Model.sharedInstance().normalAlarmRingTone

            }
            else{
                AlarmNotification.soundName                = Model.sharedInstance().normalAlarmRingTone + ".aiff"

            }
            AlarmNotification.timeZone                 = NSTimeZone.defaultTimeZone()
            AlarmNotification.fireDate                 = date11
            AlarmNotification.repeatInterval           = NSCalendarUnit.Day
            UIApplication.sharedApplication().scheduleLocalNotification(AlarmNotification)

        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
            if !(defaults.boolForKey("onorOff"))
            {
                self.saveOrUpdateAlarmFirstTimeInDB()
            }
            else
            {
                self.saveOrUpdateAlarmFirstTimeInDB()
            }
      
        self.timePickerBackgroundView?.hidden = true
        self.phoneBackgroundView?.hidden      = false
        
    }
    
    func saveOrUpdateAlarmFirstTimeInDB()
    {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let alarmInfo: Model = Model()
    
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat           = "yyyy-MM-dd"
        Model.sharedInstance().date    = formatter.stringFromDate(NSDate())
        alarmInfo.date                 = formatter.stringFromDate(NSDate())
    
       // save date in userdefaults
       defaults.setObject(model.date, forKey: "date")
       let todayDate = formatter.dateFromString( alarmInfo.date)!
    
       //Here I’m creating the calendar instance that we will operate with:
       let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    
       // asking the calendar what month and day are we in today’s date:
       let myComponents = myCalendar.components([.Weekday, .Month,], fromDate: todayDate)
       let weekDay      = myComponents.weekday
       let month        = myComponents.month
    
       alarmInfo.Day    = String(weekDay)
       alarmInfo.Month  = String(month)
       var time         = Model.sharedInstance().totalTimeForUsage
       time             = time.stringByReplacingOccurrencesOfString(":", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
       alarmInfo.TotalTime = time
       alarmInfo.Consumed  = "00.00"
    
       print("picker date",alarmInfo.date)
        
        if !(defaults.boolForKey("onorOff")) {
            // set bool value in userdefaults
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(true, forKey: "onorOff")
            
            // save today's iphone usage in data base for record.
            let isInserted = ModelManager.getInstance().addAlarmInfo(alarmInfo)
            print("data is inserted:-",isInserted)
            Model.sharedInstance().alarmsaveInDB = true
        }
        else{
            
            // save today's iphone usage in data base for record.
            let isInserted = ModelManager.getInstance().updateAlarmData(alarmInfo)
            print("data is inserted:-",isInserted)
            Model.sharedInstance().alarmsaveInDB = true
            
        }
    }
    
    @IBAction func chooseAlarm_btnAction(sender: AnyObject) {
        
        let AlarmRingToneViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AlarmRingTneVewController") as! AlarmRingTneVewController
        
        self.navigationController?.pushViewController(AlarmRingToneViewController, animated: true)
    }
    
}


