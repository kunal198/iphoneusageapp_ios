//
//  AlarmRingTneVewController.swift
//  ScreenUsages
//
//  Created by brst on 9/21/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit

class AlarmRingTneVewController: UIViewController {


    var selectToneButton = ""
    @IBOutlet weak var segmentedcontrol: UISegmentedControl!
    @IBOutlet weak var timerBorderView: UIView!
    @IBOutlet weak var segmentedControlBorderView: UIView!
    @IBOutlet weak var BorderView: UIView!
    @IBOutlet weak var firstlineView: UIView!
    @IBOutlet weak var secondlineView: UIView!
    @IBOutlet weak var selectFirstToneBorderView: UIView!
    @IBOutlet weak var selectFirstToneLbl: UILabel!
    @IBOutlet weak var selectSecondToneLbl: UILabel!
    @IBOutlet weak var selectSecondToneBorderView: UIView!
    @IBOutlet weak var selectThirdToneBorderView: UIView!
    @IBOutlet weak var selectThirdToneLbl: UILabel!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var alarmTypeBackgroundVIew: UIView!
    @IBOutlet weak var doneOrCancelBtnBackgroundView: UIView!
    
    
    override func viewDidLoad() {
        
        // hide and show timer border view accordingly
        if !(Model.sharedInstance().timeBorderViewHidden) {
              self.timerBorderView.hidden                      = true

//            self.timerBorderView.hidden                      = false
            
            if Model.sharedInstance().timerborderViewHeight  == "one" {
                self.timerBorderView.frame.size.height       = 81
                self.firstlineView.hidden                    = true
                self.BorderView.frame.size.height            = 60
                self.segmentedcontrol.selectedSegmentIndex   = 1;
            }
            else if Model.sharedInstance().timerborderViewHeight == "two"{
                self.timerBorderView.frame.size.height       = 141
                self.BorderView.frame.size.height            = 121
                self.firstlineView.hidden                    = false
                self.secondlineView.hidden                   = true
                self.segmentedcontrol.selectedSegmentIndex   = 2
            }
            else{
                self.timerBorderView.frame.size.height       = 203
                self.BorderView.frame.size.height            = 183
                self.firstlineView.hidden                    = false
                self.secondlineView.hidden                   = false
                self.segmentedcontrol.selectedSegmentIndex   = 3
            }
        }
        
        // following lines of code is used for making respected view's round corner and their borderwidth and border color
        
        self.segmentedControlBorderView.layer.cornerRadius    = 10
        self.segmentedControlBorderView.clipsToBounds         = true
        
        self.timerBorderView.layer.cornerRadius               = 10
        self.timerBorderView.clipsToBounds                    = true
        
        self.BorderView.layer.borderWidth                     = 1
        self.selectFirstToneBorderView.layer.borderWidth      = 1
        
        self.selectSecondToneBorderView.layer.borderWidth     = 1
        self.selectThirdToneBorderView.layer.borderWidth      = 1
        
        self.doneOrCancelBtnBackgroundView.layer.cornerRadius = 3
        self.doneOrCancelBtnBackgroundView.clipsToBounds      = true
        
        self.alarmTypeBackgroundVIew.layer.cornerRadius       = 10
        self.alarmTypeBackgroundVIew.clipsToBounds            = true
        
    }
    
    
    //////// math equation button action active
    @IBAction func mathEquationOnOffBtnAction(sender: AnyObject) {
        
        if switchBtn.on == true {
            Model.sharedInstance().normalAlarmRingTone = "Math Equation"
        }
        else{
            
            if Model.sharedInstance().normalAlarmRingTone == "Math Equation" {
                Model.sharedInstance().normalAlarmRingTone = "AppleUpdate"
            }

        }
        
    }
    

    //////// done or cancel button action
    @IBAction func doneOrCancelBtnAction(sender: AnyObject) {
        
        let alert :UIAlertView  = UIAlertView()
        alert.title = ""
        alert.message = "Alarm Tone Saved succesfully"
        alert.addButtonWithTitle("OK")
        alert.show()
        
    }
    
    
    ///////// voice recording button action
    @IBAction func voiceRecordingBtnAction(sender: AnyObject) {
        
        let recordingViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("RecordingViewcontroller") as!RecordingViewcontroller
        recordingViewcontroller.selectedToneFor = "NormalAlarmTone"
        self.navigationController?.pushViewController(recordingViewcontroller, animated: true)
        
    }
    
    
    ///////// ring tone button button action
    @IBAction func ringToneBtnAction(sender: AnyObject) {
        
        let ringtoneViewController             = self.storyboard?.instantiateViewControllerWithIdentifier("RingtoneViewController") as! RingtoneViewController
        ringtoneViewController.selectedToneFor = "NormalAlarmTone"
        self.navigationController?.pushViewController(ringtoneViewController, animated: true)
        
    }
    
    @IBAction func backbtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
   
}
