//
//  AudioRecordingsViewController.swift
//  ScreenUsages
//
//  Created by brst on 10/5/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit
import AVFoundation


class AudioRecordingsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    var selectedToneFor :NSString = ""
    var fileName = ""
    var audioPlay :Bool = false
    var indexPath :Int = 0
    
    ///Model singleton
    let model = Model.sharedInstance()
    
    // var recordings = [NSURL]()
    var player:AVAudioPlayer!

    
    
    
    @IBOutlet weak var tableVIew: UITableView!
    
    @IBOutlet weak var tableVewBackground_Vew: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listRecordings()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    // MARK: Section Info
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 0
        
    }
    
//    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        return "Ringtones"
//    }
    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 44
//    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:RingToneTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! RingToneTableViewCell
//        
//        cell.ringtoneName_Lbl?.text = songList[indexPath.row] as? String
        
        if #available(iOS 9.0, *) {
            tableView.remembersLastFocusedIndexPath = true
        } else {
            // Fallback on earlier versions
        };
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }

    @IBAction func backButton_action(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func saveButtonAction(sender: AnyObject) {
    }
    
    func listRecordings() {
        
        let appDir = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0]
        let path  = appDir + "/Sounds"
        let fileUrl = NSURL(fileURLWithPath: path)
        
        do {
            let urls = try NSFileManager.defaultManager().contentsOfDirectoryAtURL(fileUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsHiddenFiles)
            Model.sharedInstance().recordings = urls.filter( { (name: NSURL) -> Bool in
                
                return name.lastPathComponent!.hasSuffix("aiff")
            })
            
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("something went wrong listing recordings")
        }
        
    }

}
