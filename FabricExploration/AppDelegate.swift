//
//  AppDelegate.swift
//  ScreenUsages
//
//  Created by Shikha Kamboj on 1/08/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

////TOTAL/WEEKLY AVG 


import UIKit
import AVFoundation

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
     var reminderType = ""
     var ringtonetype = ""
     var soundName    = ""
    
     var startTime = NSTimeInterval(0)  // start time is used for storing the value of start timer time
    
     var accumulatedTime = NSTimeInterval(0) // accumulated time is used for storing the time where timer get stopped
    
     var elapsedSinceLastRefresh = NSTimeInterval(0)
    
     var timer = NSTimer()  // this is timer variable
    
     var elapsedTime = NSTimeInterval(0)  // elapsed time is used for storing time elapsed
    
     var updateBlock: ((NSTimeInterval, NSTimeInterval?) -> ())? // this method is used for updating the time on the label..
    
    var totaltime : Float = 0 // Total time is  used for showing progress on the circular progress view.
    
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.stringFromDate(NSDate())
        
        print("booolllll",defaults.boolForKey("onorOff"))
        if (defaults.boolForKey("onorOff")) {
            let previousDate : NSString = (defaults.stringForKey("date"))!
            
            print(previousDate)
            if (previousDate != date)  {
                defaults.setBool(false, forKey: "onorOff")
            }
        }
        
        // Override point for customization after application launch.

        // following method call is used for checking the lock or unlock state of the app.
        DarwinNotificationsManager.sharedInstance().registerforDeviceLockNotif()
            LocationManager.sharedManager.startUpdatingLocation()
        
        /// data base
        Util.copyFile("iPhoneUsageDB.sqlite")
        
        return true
    }

    // Used for checking whether notification is registered or not
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        
        print(notificationSettings.types.rawValue)
        
    }
    
    // call when local Notification received in active mode
    // call when local Notification received in active mode
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        self.setSessionPlayback()

        if #available(iOS 8.2, *) {
            soundName  = notification.alertTitle! as String
        } else {
            // Fallback on earlier versions
        }
        let msg  = notification.alertBody! as String
        
        if soundName == "Math Equation" {
            let alert : UIAlertView = UIAlertView()
            alert.message = "You need to solve a math Equation"
            alert.addButtonWithTitle("OK")
            alert.delegate = self
            alert.tag = 1
            alert.show()
        }
        else{
         
            if Model.sharedInstance().appcomesFromBackground == false {
                // Do something serious in a real app.
                print("Received Local Notification:")
                
                let str = soundName
                
                let delimiter = ".aiff"
                
                var token = str.componentsSeparatedByString(delimiter)
                print (token[0])
                
                var audioPath = ""
                
                let result1 = str.containsString("Sounds")

                if result1  {
                    let fileUrl =  NSURL(string: soundName)
                    play(fileUrl!)
                }
                else{
                    audioPath = NSBundle.mainBundle().pathForResource(token[0] as String, ofType: "aiff")!
                    do {
                        
                        let fileURL = NSURL(fileURLWithPath: audioPath)
                        Model.sharedInstance().audioPlayer = try AVAudioPlayer(contentsOfURL: fileURL, fileTypeHint: nil)
                        Model.sharedInstance().audioPlayer.prepareToPlay()
                        Model.sharedInstance().audioPlayer.play()
                        
                    }
                    catch {
                        print("Something bad happened. Try catching specific errors to narrow things down")
                    }
                
                }
                
                let alert : UIAlertView = UIAlertView()
                alert.message = msg
                alert.addButtonWithTitle("OK")
                alert.delegate = self
                alert.tag = 2
                alert.show()

            }
            
        }
        
    }

    func applicationWillResignActive(application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(application: UIApplication) {
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    func play(url:NSURL) {
        print("playing \(url)")
        
        do {
            Model.sharedInstance().audioPlayer = try AVAudioPlayer(contentsOfURL: url)
            Model.sharedInstance().audioPlayer.prepareToPlay()
            Model.sharedInstance().audioPlayer.volume = 5.0
            Model.sharedInstance().audioPlayer.play()
        } catch let error as NSError {
           // Model.sharedInstance().audioPlayer = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
    
    // alert View action method
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
        
        switch View.tag{
        
        case 1:
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            homeViewController.soundName = soundName
            let nav = UINavigationController(rootViewController: homeViewController)
            
            self.window!.rootViewController = nav
            break;
        case 2:
            
            Model.sharedInstance().audioPlayer.stop()
            
            break;
        default:
            NSLog("Default");
            break;
            
        }
    }
    
    // make session active
    func setSessionPlayback() {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch let error as NSError {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch let error as NSError {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    // handle notification action 
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        
        if identifier == "EnableReminder" {
        
        let soundName = notification.soundName
            if soundName == "Math Equation" {
                ringtonetype = "Math Equation"
                reminderType = "NoReminder"
            }
        }
 
//            if Model.sharedInstance().firstRingToneType != ""
//            {
//            ringtonetype = Model.sharedInstance().firstRingToneType as String
//            print("ringtonetype ::::",ringtonetype)
//            Model.sharedInstance().firstRingToneType = ""
//            }
//            else if Model.sharedInstance().secondRingToneType != ""
//            {
//            ringtonetype = Model.sharedInstance().secondRingToneType as String
//                print("hgdhdh",ringtonetype)
//            Model.sharedInstance().secondRingToneType = ""
//            }
//            else if Model.sharedInstance().thirdRinngToneType != ""
//            {
//            ringtonetype = Model.sharedInstance().thirdRinngToneType as String
//            Model.sharedInstance().thirdRinngToneType = ""
//            }
//            
//            reminderType = "NoReminder"
//       NSNotificationCenter.defaultCenter().postNotificationName("modifyListNotification", object: nil)
//            
//        }
//        else
//        {
//            if Model.sharedInstance().firstRingToneType != ""
//            {
//                ringtonetype = Model.sharedInstance().firstRingToneType as String
//            }
//            else if Model.sharedInstance().secondRingToneType != ""
//            {
//                ringtonetype = Model.sharedInstance().secondRingToneType as String
//            }
//            else if Model.sharedInstance().thirdRinngToneType != ""
//            {
//                ringtonetype = Model.sharedInstance().thirdRinngToneType as String
//            }
//            reminderType = "NoReminder"
//        }
//        
        completionHandler()
    }
    
    // Returns an array of unique random numbers with range
    
    func getRandomNumbersOfSize(size: Int, from lowerLimit: Int, to higherLimit: Int) -> [NSMutableArray] {
        let uniqueNumbers = NSMutableArray()
        var r: Int
        
        while uniqueNumbers.count < size {
            
            let max = UInt32(higherLimit)
            let min = UInt32(lowerLimit)
            
            r = Int(min + arc4random_uniform(max - min + 1))
            
            if !uniqueNumbers.containsObject(Int(r)) {
                uniqueNumbers.addObject(Int(r))
            }
        }
        return [uniqueNumbers]
    }
    
    // code for getting rendom expression
    func getrandomExpression() -> String {
        
        var rndValue = 100 + arc4random() % (500 - 100)
        var expression = ""
        var n : NSInteger = 0
        var numberarray = [AnyObject]()
        
        while rndValue > 0 {
            n = Int(rndValue % 10)
            rndValue /= 10
            print("\(n)")
            let myString = "\(n)"
            numberarray.append(myString)
        }
        
        var operators = ["+", "-", "*"]
        var randomOperatorIndexArray = self.getRandomNumbersOfSize(3, from: 0, to: (operators.count - 1))
        expression = numberarray[2] as! String
        for j in 0..<3 {
            expression = "\(expression)\(numberarray[j])"
            let i = randomOperatorIndexArray[0]
            let index = i[j]
            print("print iiii",i)
            let randomOperator = operators[Int(index as! NSNumber)]
            expression = "\(expression)\(randomOperator)"
            print("\(randomOperator)")
            print("\(expression)")
            if j == 2 {
                expression = "\(expression)\(numberarray[0])"
                print("\(expression)")
            }
        }
        
        return expression
    }
    
}
