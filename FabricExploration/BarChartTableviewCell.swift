//
//  BarChartTableviewCell.swift
//  ScreenUsages
//
//  Created by brst on 9/3/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import Foundation
import UIKit

class BarChartTableviewCell: UITableViewCell, UUChartDataSource
{
    var consumedTimeArray : NSMutableArray!
    var totalTimeArray :NSMutableArray!
    var dayArray : NSMutableArray!
    
    var chartView: UUChart!
    
    var path: NSIndexPath!
    
    func configUI(indexPath: NSIndexPath) {
        if chartView != nil {
            chartView!.removeFromSuperview()
            chartView = nil
        }
        
        path = indexPath
        
        chartView = UUChart(frame: CGRectMake(10, 3, UIScreen.mainScreen().bounds.size.width - 20, self.frame.height-70), dataSource: self, style: .Bar)
        
        self.contentView.backgroundColor = UIColor.clearColor()
        
        chartView!.showInView(self.contentView)
        
    }
    
    func getXTitles(num: Int) -> [AnyObject] {
        
        var xTitles = [AnyObject]()
        for i in 0..<num {
            
            let str = dayArray[i]
            
            xTitles.append(str)
        }
        return xTitles
        
    }
    
    func chartConfigAxisXLabel(chart: UUChart) -> [AnyObject] {
        
        return self.getXTitles(dayArray.count)

    }
    
    func chartConfigAxisYValue(chart: UUChart) -> [AnyObject] {
        
      Model.sharedInstance().consumedTimeArray = [] // used for showing y values on the bar chart
        
      Model.sharedInstance().totalTimeArray = []
        
       dayArray = []
        
        for i in 0...Model.sharedInstance().marrStudentData.count-1
        {
            
        Model.sharedInstance().consumedTimeArray.addObject(Model.sharedInstance().marrStudentData.objectAtIndex(i).valueForKey("Consumed")!)
            
        Model.sharedInstance().totalTimeArray.addObject(Model.sharedInstance().marrStudentData.objectAtIndex(i).valueForKey("TotalTime")!)
        
            var day = String(Model.sharedInstance().marrStudentData.objectAtIndex(i).valueForKey("Day")!)
           
            if day == "1"
            {
                day = "Sun"
            }
            else if day == "2"
            {
              day = "Mon"
            }
            else if day == "3"
            {
              day = "Tue"
            }
            else if day == "4"
            {
              day = "Wed"
            }
            else if day == "5"
            {
              day = "Thu"
            }
            else if day == "6"
            {
              day = "Fri"
            }
            else if day == "7"
            {
              day = "Sat"
            }
            
            dayArray.addObject(day)
            
        }
        print("ConsumedArray",Model.sharedInstance().consumedTimeArray)
        print("TotalTimeArray",Model.sharedInstance().totalTimeArray)
        return [Model.sharedInstance().consumedTimeArray]
    }
    
    func chartConfigColors(chart: UUChart) -> [AnyObject] {
        return [UUColor.green(), UUColor.red(), UUColor.yellow()]
    }
    
    func chartRange(chart: UUChart) -> CGRange {
        return CGRangeMake(12, 0)
    }
    
    func chartHighlightRangeInLine(chart: UUChart) -> CGRange {
        return CGRangeMake(25, 75)
    }
    
    func chart(chart: UUChart, showHorizonLineAtIndex index: Int) -> Bool {
        return true
    }
    
    func chart(chart: UUChart, showMaxMinAtIndex index: Int) -> Bool {
        return path.row == 2
    }
}
