//
//  SettingViewController.swift
//  ScreenUsages
//
//  Created by brst on 8/11/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    @IBOutlet weak var backGroundView: UIView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // following line of code is used for making round corner of backgroundView
            self.backGroundView.layer.cornerRadius =  10
            self.backGroundView.clipsToBounds = true

    }
    
    // following button Action is used for moving on the respected screens of button Specified
    
    @IBAction func settingScreenBtnAction(sender: AnyObject) {
        
        let buttonTag = sender.tag
        
        if buttonTag == 0 {
            let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("ParentControllViewController") as!ParentControllViewController
            self.navigationController?.pushViewController(reminder, animated: true)
        }
            
       else if buttonTag == 1 {
            let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("Reminder") as!Reminder
            if Model.sharedInstance().alarmSet {
                self.navigationController?.pushViewController(reminder, animated: true)
            }
            else
            {
                let alertView :UIAlertView = UIAlertView()
                alertView.delegate = self
                alertView.title = "Warning!"
                alertView.message = "You can not set reminder before setting alarm"
                alertView.addButtonWithTitle("Ok")
                alertView.tag = 0
                alertView.show()
            }
        }
            
        else if buttonTag == 2 {
            
            if Model.sharedInstance().passwordSaved {
                let phoneLockViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("PhoneLockViewController") as!PhoneLockViewController
                phoneLockViewcontroller.canSetAlarm = true
                self.navigationController?.pushViewController(phoneLockViewcontroller, animated: true)
            }
            else
            {
                let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("AlarmViewController") as!AlarmViewController
                self.navigationController?.pushViewController(reminder, animated: true)
            }
        }
        else if buttonTag == 3 {
            let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("VoiceRecoderViewController") as!VoiceRecoderViewController
            self.navigationController?.pushViewController(reminder, animated: true)
        }
        else if buttonTag == 4
        {
            let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("Reminder") as!Reminder
            self.navigationController?.pushViewController(reminder, animated: true)
        }
    }
    
    // Back Button Action
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}