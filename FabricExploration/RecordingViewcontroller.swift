//
//  RecordingCollectionViewcontroller.swift
//  ScreenUsages
//
//  Created by brst on 8/19/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class RecordingViewcontroller: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedToneFor :NSString = ""
    var fileName = ""
    var audioPlay :Bool = false
    var indexPath :Int = 0

    ///Model singleton
    let model = Model.sharedInstance()

   // var recordings = [NSURL]()
    var player:AVAudioPlayer!
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // set the recordings array
        listRecordings()
        
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(RecordingViewcontroller.longPress(_:)))
        recognizer.minimumPressDuration = 0.5 //seconds
        recognizer.delegate = self
        recognizer.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(recognizer)
        
        let doubleTap = UITapGestureRecognizer(target:self, action:#selector(RecordingViewcontroller.doubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        doubleTap.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(doubleTap)

    }
    
    func doubleTap(rec:UITapGestureRecognizer) {
        if rec.state != .Ended {
            return
        }
        
        let p = rec.locationInView(self.collectionView)
        if let indexPath = self.collectionView?.indexPathForItemAtPoint(p) {
            askToRename(indexPath.row)
        }
        
    }
    
    func longPress(rec:UILongPressGestureRecognizer) {
        if rec.state != .Ended {
            return
        }
        let p = rec.locationInView(self.collectionView)
        if let indexPath = self.collectionView?.indexPathForItemAtPoint(p) {
            askToDelete(indexPath.row)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Model.sharedInstance().recordings.count
    }
    
     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! RecordingCollectionViewCell
        
        cell.label.text = Model.sharedInstance().recordings[indexPath.row].lastPathComponent
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
     func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
     func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
     func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        audioPlay = true
        self.setSessionPlayback()
        
        let str = String(Model.sharedInstance().recordings[indexPath.row])
        var strSplit = str.componentsSeparatedByString(".aiff")
        
        fileName  = strSplit[0]

        play(Model.sharedInstance().recordings[indexPath.row])
        self.indexPath = indexPath.row
        
        print(fileName)
        
    }
    
    func play(url:NSURL) {
        print("playing \(url)")
        
        do {
            self.player = try AVAudioPlayer(contentsOfURL: url)
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
        } catch let error as NSError {
            self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
    func listRecordings() {
        
        let appDir = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0]
        let path  = appDir + "/Sounds"
        let fileUrl = NSURL(fileURLWithPath: path)
      
        do {
            let urls = try NSFileManager.defaultManager().contentsOfDirectoryAtURL(fileUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsHiddenFiles)
          Model.sharedInstance().recordings = urls.filter( { (name: NSURL) -> Bool in
            
                return name.lastPathComponent!.hasSuffix("aiff")
            })
            
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("something went wrong listing recordings")
        }
        
    }
    
    func askToDelete(row:Int) {
        
        let alert = UIAlertController(title: "Delete",
                                      message: "Delete Recording \(Model.sharedInstance().recordings[row].lastPathComponent!)?",
                                      preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {action in
            print("yes was tapped \(Model.sharedInstance().recordings[row])")
            self.deleteRecording(Model.sharedInstance().recordings[row])
        }))
        alert.addAction(UIAlertAction(title: "No", style: .Default, handler: {action in
            print("no was tapped")
        }))
        self.presentViewController(alert, animated:true, completion:nil)
        
    }
    
    func askToRename(row:Int) {
        
        let recording = Model.sharedInstance().recordings[row]
        
        let alert = UIAlertController(title: "Rename",
                                      message: "Rename Recording \(recording.lastPathComponent!)?",
                                      preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {[unowned alert] action in
            print("Yes was tapped \(Model.sharedInstance().recordings[row])")
            if let textFields = alert.textFields{
                let tfa = textFields as [UITextField]
                let text = tfa[0].text
                let url = NSURL(fileURLWithPath: text!)
                self.renameRecording(recording, to: url)
            }
            }))
        
        alert.addAction(UIAlertAction(title: "No", style: .Default, handler: {action in
            print("no was tapped")
        }))
        
        alert.addTextFieldWithConfigurationHandler({textfield in
            textfield.placeholder = "Enter a filename"
            textfield.text = "\(recording.lastPathComponent!)"
        })
        
        self.presentViewController(alert, animated:true, completion:nil)
        
    }
    
    func renameRecording(from:NSURL, to:NSURL) {
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let toURL = documentsDirectory.URLByAppendingPathComponent(to.lastPathComponent!)
        
        print("renaming file \(from.absoluteString) to \(to) url \(toURL)")
        let fileManager = NSFileManager.defaultManager()
        fileManager.delegate = self
        do {
            try NSFileManager.defaultManager().moveItemAtURL(from, toURL: toURL)
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error renaming recording")
        }
        dispatch_async(dispatch_get_main_queue(), {
            self.listRecordings()
            self.collectionView?.reloadData()
        })
    }
    
    func deleteRecording(url:NSURL) {
        
        print("removing file at \(url.absoluteString)")
        let fileManager = NSFileManager.defaultManager()
        
        do {
            try fileManager.removeItemAtURL(url)
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error deleting recording")
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.listRecordings()
            self.collectionView?.reloadData()
        })
    }
    
    @IBAction func saveButtonAction(sender: AnyObject) {
        
        if audioPlay{
            // save alarm tone for normal alarm
            if selectedToneFor == "NormalAlarmTone" {
                Model.sharedInstance().normalAlarmRingTone = fileName
            }
            
            //save tone for showing on labels
            if selectedToneFor == "FirstToneButton" {
                Model.sharedInstance().firstRingToneType     = "systemRingtone"
                Model.sharedInstance().firstReminderRingTone = fileName
                Model.sharedInstance().firstTone             = fileName
            }
            else if selectedToneFor == "SecondToneButton"{
                Model.sharedInstance().secondRingToneType     = "systemRingtone"
                Model.sharedInstance().secondReminderRingTone = fileName
                Model.sharedInstance().secondTone             = fileName
            }
            else if selectedToneFor == "ThirdToneButton" {
                Model.sharedInstance().thirdRinngToneType      = "systemRingtone"
                Model.sharedInstance().thirdReminderRingTone   = fileName
                Model.sharedInstance().thirdTone               = fileName
            }
            
            // set selected ringtone for Reminder
            if audioPlay {
                self.player.stop()
            }
            
            let alert : UIAlertView = UIAlertView()
            alert.title   = ""
            alert.message = "Alarm set successfully"
            alert.addButtonWithTitle("OK")
            alert.show()
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        
     }
    
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setSessionPlayback() {
        
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch let error as NSError {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch let error as NSError {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
}

// MARK: AVAudioPlayerDelegate
extension RecordingViewcontroller: NSFileManagerDelegate {
    
    func fileManager(fileManager: NSFileManager, shouldMoveItemAtURL srcURL: NSURL, toURL dstURL: NSURL) -> Bool {
        
        print("should move \(srcURL) to \(dstURL)")
        return true
        
    }
}

extension RecordingViewcontroller : UIGestureRecognizerDelegate {
    
}