//
//  RingtoneViewController.swift
//  ScreenUsages
//
//  Created by brst on 8/17/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class RingtoneViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    
    var songList : NSArray = NSArray()
    var  selectedToneFor :NSString  = ""
    var  fileName = ""
    var audioPlay :Bool = false
    
    // MARK: - Class Variables
    
    ///Model singleton
    let model = Model.sharedInstance()
    
    ///The directories where sound files are located.
    let rootSoundDirectories: [String] = ["/Library/Ringtones", "/System/Library/Audio/UISounds"]
    
    ///Array to hold directories when we find them.
    var directories: [String] = []
    
    ///Tuple to hold directories and an array of file names within.
    var soundFiles: [(directory: String, files: [String])] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        songList = ["AppleUpdate","iPhone5Original","iPhone5c","iPhone_5s_Ringtone1","iPhone_5s_Ringtone","iPhone_7_Leaked","iPhone_Charming","iPhone_Marimba_Remix","iPhone_Broken_Robot","iPhone_Chillin","iPhone_Future_Connect","iPhone_Just_Vibes","iPhone_Loft_Nine","iPhone_Lounge_Vibes","iPhone_New_Old_Jeans"];
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        for directory in rootSoundDirectories {
            directories.append(directory)
            
            let newSoundFile: (directory: String, files: [String]) = (directory, [])
            soundFiles.append(newSoundFile)
        }
        
       getDirectories()
       loadSoundFiles()
        
    }
    
    //
    // MARK: - Class Functions
    /**
     Starting with the "/Library/Ringtones" & "/System/Library/Audio/UISounds" directories, it looks for other sub-directories just one level lower and saves their relative path in directories array.
     
     - URLs: All of the contents of the directory (files and sub-directories).
     */
    
    func getDirectories() {
        let fileManager: NSFileManager = NSFileManager()
        for directory in rootSoundDirectories {
            let directoryURL: NSURL = NSURL(fileURLWithPath: "\(directory)", isDirectory: true)
            
            do {
                if let URLs: [NSURL] = try fileManager.contentsOfDirectoryAtURL(directoryURL, includingPropertiesForKeys: [NSURLIsDirectoryKey], options: NSDirectoryEnumerationOptions()) {
                    var urlIsaDirectory: ObjCBool = ObjCBool(false)
                    for url in URLs {
                        if fileManager.fileExistsAtPath(url.path!, isDirectory: &urlIsaDirectory) {
                            if urlIsaDirectory {
                                let directory: String = "\(url.relativePath!)"
                                let files: [String] = []
                                let newSoundFile: (directory: String, files: [String]) = (directory, files)
                                directories.append("\(directory)")
                                soundFiles.append(newSoundFile)
                            }
                        }
                    }
                }
            } catch {
                debugPrint("\(error)")
            }
        }
    }
    
    /**
     For each directory, it looks at each item (file or directory) and only appends the sound files to the soundfiles[i]files array.
     
     - URLs: All of the contents of the directory (files and sub-directories).
     */
    
    func loadSoundFiles() {
        for i in 0...directories.count-1 {
            let fileManager: NSFileManager = NSFileManager()
            let directoryURL: NSURL = NSURL(fileURLWithPath: directories[i], isDirectory: true)
            
            do {
                if let URLs: [NSURL] = try fileManager.contentsOfDirectoryAtURL(directoryURL, includingPropertiesForKeys: [NSURLIsDirectoryKey], options: NSDirectoryEnumerationOptions()) {
                    var urlIsaDirectory: ObjCBool = ObjCBool(false)
                    for url in URLs {
                        if fileManager.fileExistsAtPath(url.path!, isDirectory: &urlIsaDirectory) {
                            if !urlIsaDirectory {
                                soundFiles[i].files.append("\(url.lastPathComponent!)")
                            }
                        }
                    }
                }
            } catch {
                debugPrint("\(error)")
            }
        }
    }
    
    // MARK: - Table view data source
    // MARK: Section Info
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
      
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
        return songList.count
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Ringtones"
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:RingToneTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! RingToneTableViewCell

        cell.ringtoneName_Lbl?.text = songList[indexPath.row] as? String
        
        if #available(iOS 9.0, *) {
            tableView.remembersLastFocusedIndexPath = true
        } else {
            // Fallback on earlier versions
        };

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
       audioPlay = true
       self.setSessionPlayback()

       fileName = songList[indexPath.row] as! String
       let audioPath = NSBundle.mainBundle().pathForResource(songList[indexPath.row] as? String, ofType: "aiff")
        do {
            
            let fileURL = NSURL(fileURLWithPath: audioPath!)
            Model.sharedInstance().audioPlayer = try AVAudioPlayer(contentsOfURL: fileURL, fileTypeHint: nil)
            Model.sharedInstance().audioPlayer.prepareToPlay()
            Model.sharedInstance().audioPlayer.delegate = self
            Model.sharedInstance().audioPlayer.play()
            
        }
        catch {
            print("Something bad happened. Try catching specific errors to narrow things down")
        }
        
    }

    // following Button is used for saving the selected song for alarmTone and move on last screen
    @IBAction func saveButtonAction(sender: AnyObject) {
        
        // save alarm tone for normal alarm
        
        if selectedToneFor == "NormalAlarmTone" {
            Model.sharedInstance().normalAlarmRingTone = fileName
        }
        
        //save tone for showing on labels
        if selectedToneFor == "FirstToneButton" {
            Model.sharedInstance().firstRingToneType     = "systemRingtone"
            Model.sharedInstance().firstReminderRingTone = fileName
            Model.sharedInstance().firstTone             = fileName
        }
        else if selectedToneFor == "SecondToneButton"{
           Model.sharedInstance().secondRingToneType     = "systemRingtone"
           Model.sharedInstance().secondReminderRingTone = fileName
           Model.sharedInstance().secondTone             = fileName
        }
        else if selectedToneFor == "ThirdToneButton" {
          Model.sharedInstance().thirdRinngToneType      = "systemRingtone"
          Model.sharedInstance().thirdReminderRingTone   = fileName
          Model.sharedInstance().thirdTone               = fileName
        }
        
        // set selected ringtone for Reminder
        if audioPlay {
            if  Model.sharedInstance().audioPlayer.playing {
                Model.sharedInstance().audioPlayer.stop()
            }
        }
        
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //Following button used for canceling the selection
    @IBAction func cancelButtonAction(sender: AnyObject) {
        
        if audioPlay {
            
            if  Model.sharedInstance().audioPlayer.playing {
                Model.sharedInstance().audioPlayer.stop()
            }
        }
        
       self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setSessionPlayback() {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch let error as NSError {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch let error as NSError {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }

 }
