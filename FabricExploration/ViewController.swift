//
//  ViewController.swift
//  ScreenUsages
//
//  Created by Shikha Kamboj
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class ViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate,UITableViewDataSource, AVAudioPlayerDelegate {
    
    var soundName : NSString = ""
    @IBOutlet weak var totalConsumedhour_lbl: UILabel!
    @IBOutlet weak var totalHour_lbl: UILabel!
    //notification Vew
    @IBOutlet weak var totalAvg_lbl: UILabel!
    @IBOutlet weak var mathEquation_Vew: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var okBtn_btn: UIButton!
    @IBOutlet weak var mathEquationResult_txt: UITextField!
    @IBOutlet weak var randomExpression_lbl: UILabel!
    
    //Data retrieve bool
    var dataRetrived :Bool = false

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var barchartViewBg: UIView!
    @IBOutlet weak var progressViewBck: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var progressviewBackgroundView: UIView!
    
    // bool for making alarm off
    var alarmGoingOff: Bool = false
    
  //  @IBOutlet weak var mathBackGroundView: UIView!
    
    @IBOutlet weak var clockInnerCircle_imgVew: UIImageView!  // used  for showing inner circle image
    
    @IBOutlet weak var circleProgressView: CircleProgressView! // object used for calling the methods of the respective class
    
    let nf = NSNumberFormatter()
    
    @IBOutlet weak var totalTimeToUse_lBL: UILabel!
    
    @IBOutlet weak var chronometerLabel: UILabel! // used for showing the time
    
    @IBOutlet weak var statusLbl: UILabel! // used for showing the status of the iphone usage
    
    var chronometer: Chronometer! // used for calling the methods of chronometer class
    
    let totalPages = 2

    override func viewDidLoad() {
        
        if (Model.sharedInstance().timesortedArray.count == 0)
        {
            Model.sharedInstance().timesortedArray = NSMutableArray()
        }
        
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.appDidBecomeActive), name: UIApplicationDidBecomeActiveNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(self.appWillEnterForeground), name:UIApplicationWillEnterForegroundNotification, object:nil)

        // set localnotification settings
        setupNotificationSettings()

        nf.numberStyle           = NSNumberFormatterStyle.DecimalStyle
        nf.maximumFractionDigits = 2
        
        // following line of code is used for making round corner of a label
        self.statusLbl.clipsToBounds      = true
        self.statusLbl.layer.cornerRadius = 20.0
        
        // following line of code is used for making round corner of a math equation view
        self.mathEquation_Vew.clipsToBounds      =  true
        self.mathEquation_Vew.layer.cornerRadius = 20.0
        
        // make ok button corner round
        self.okBtn_btn.clipsToBounds      = true
        self.okBtn_btn.layer.cornerRadius = 5.0
        
    }

    func appWillEnterForeground(notification: NSNotification) {
    
       Model.sharedInstance().appcomesFromBackground = true
        
    }
    
    func appDidBecomeActive(notification: NSNotification) {
      
        Model.sharedInstance().appcomesFromBackground = false
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let alarmInfo: Model = Model()
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.stringFromDate(NSDate())
        let todayDate = formatter.dateFromString(date)!
        
        //Here I’m creating the calendar instance that we will operate with:
        // asking the calendar what month are we in today’s date:
        
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components([.Weekday, .Month,], fromDate: todayDate)
        
        let month = myComponents.month
        alarmInfo.Month = String(month)
        
        // Retrive data from the data base for showing on bar chart
        
        Model.sharedInstance().marrStudentData = NSMutableArray()
        Model.sharedInstance().marrStudentData = ModelManager.getInstance().getAllAlarmData(alarmInfo)
        
        if (Model.sharedInstance().marrStudentData.count > 0) {
            dataRetrived = true
            self.tableView.reloadData()
        }
        
        // update average time
        
        var sum: Float = 0
        for i in 0..<Model.sharedInstance().consumedTimeArray.count {
            sum = sum + (Model.sharedInstance().consumedTimeArray[i]).floatValue
        }
        
        let count : Float = Float(Model.sharedInstance().consumedTimeArray.count)
        sum = sum / count
        print("average   \(sum)")
        
        let average = String(format: "TOTAL/WEEKLY AVG %f", sum)
        self.totalAvg_lbl.text = average
        
        
        // if app open on next day,, following code is used for restart the timer from begining
        
        if Model.sharedInstance().date != "" {
            
            if Model.sharedInstance().date != date
            {
                
                var time = (self.chronometerLabel?.text)!
                time = time.stringByReplacingOccurrencesOfString(":", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
                alarmInfo.Consumed = time
                
                // update today's iphone usage in data base for record.
                let isInserted = ModelManager.getInstance().updateConsumedTime(alarmInfo)
                print("data is inserted:-",isInserted)
                
                let image1 = UIImage(named: "clock-ring2.png")
                self.clockInnerCircle_imgVew.image    = image1
                Model.sharedInstance().totalTimeBool  = false
                appDelegate.totaltime                 = 0.0
                chronometer.reset()
                chronometer.start()
                
            }
        }
        
        ///// following code used for showing math equation as reminder
        
        if appDelegate.reminderType == "NoReminder" {
            
            if appDelegate.ringtonetype == "Math Equation"
            {
                
                let expression = getrandomExpression()
                randomExpression_lbl?.text = expression
                
                mathEquation_Vew.hidden    = false
                visualEffectView.hidden    = false
                
                appDelegate.ringtonetype   = ""
            }
            
            else if Model.sharedInstance().normalAlarmRingTone == "Math Equation"{
                
                let expression             = getrandomExpression()
                randomExpression_lbl?.text = expression
                
                mathEquation_Vew.hidden    = false
                visualEffectView.hidden    = false
                
            }
        }
        
        print("did become active notification")
    }
    
    override func viewDidAppear(animated: Bool)
    {
       // tableView.backgroundColor = UIColor.greenColor()
        // set total time on bar chart
        self.totalHour_lbl.text = Model.sharedInstance().totalTimeForUsage
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
            if soundName == "Math Equation"{
                
                let expression = getrandomExpression()
                randomExpression_lbl?.text = expression
                
                mathEquation_Vew.hidden = false
                visualEffectView.hidden = false
                soundName = ""
                
            }
        
        let alarmInfo: Model = Model()
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.stringFromDate(NSDate())
        let todayDate = formatter.dateFromString(date)!
        
        //Here I’m creating the calendar instance that we will operate with:
        // asking the calendar what month are we in today’s date:
        
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components([.Weekday, .Month,], fromDate: todayDate)
        
        let month = myComponents.month
        alarmInfo.Month = String(month)
        
        if Model.sharedInstance().date != "" {

            if Model.sharedInstance().date != date
            {
                
                let image1 = UIImage(named: "clock-ring2.png")
                self.clockInnerCircle_imgVew.image = image1
                Model.sharedInstance().totalTimeBool = false
                appDelegate.totaltime = 0.0
                chronometer.start()

                var time = (self.chronometerLabel?.text)!
                time = time.stringByReplacingOccurrencesOfString(":", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
                alarmInfo.Consumed = time
                
                // update today's iphone usage in data base for record.
                let isInserted = ModelManager.getInstance().updateConsumedTime(alarmInfo)
                print("data is inserted:-",isInserted)
            
            }
        }
        
        // Retrive data from the data base for showing on bar chart
        
        Model.sharedInstance().marrStudentData = NSMutableArray()
        Model.sharedInstance().marrStudentData = ModelManager.getInstance().getAllAlarmData(alarmInfo)
        
        if (Model.sharedInstance().marrStudentData.count > 0) {
            dataRetrived = true
            self.tableView.reloadData()
        }

        //configure scrollView and PageControl
        configureScrollView()
        configurePageControl()
        
        // Set total time usage for a day
        self.totalTimeToUse_lBL.text = Model.sharedInstance().totalTimeForUsage
        
        // following if condition is used for setting circle's image
        if Model.sharedInstance().totalTimeForUsage != "00:00" {
            
            if Model.sharedInstance().totalTimeBool {
                
                let image1 = UIImage(named: "clock-ring2.png")
                self.clockInnerCircle_imgVew.image = image1
                Model.sharedInstance().totalTimeBool                 = false
                appDelegate.totaltime              = 0.0
                
            }
            
                self.chronometer = Chronometer(refreshInterval: NSTimeInterval(0.01)){
                (elapsedTime: NSTimeInterval, remainingTime: NSTimeInterval?) in
                
                if let r = remainingTime {
                    
                   self.chronometerLabel.text = r.getFormattedInterval(miliseconds: true)
                    
               }
               else{
               
                    // following code is used for stop progress on circular progress view and stop label to update if time is over
                    
                    if appDelegate.totaltime < 1.000000
                    {
                
                        self.chronometerLabel.text = elapsedTime.getFormattedInterval(miliseconds: true)
                        
                        self.totalConsumedhour_lbl.text = self.chronometerLabel.text
                        
                        alarmInfo.date = Model.sharedInstance().date
                        
                        var time       = (self.chronometerLabel?.text)!
                        
                        time           = time.stringByReplacingOccurrencesOfString(":", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
                        
                        alarmInfo
                             .Consumed = time
                        
                        // update today's iphone usage in data base for record.
                         ModelManager.getInstance().updateConsumedTime(alarmInfo)
                        
                        if(Model.sharedInstance().reminderSet){
                        
                          let totaltime : [String] = time.componentsSeparatedByString(".")
                          let hours     : Int      = Int(totaltime[0])!
                          let minutes   : Int      = Int(totaltime[1])!
                          let seconds   : Int      = Int(totaltime[2])!
                            
                          let totaltimeElapsed : Int = hours*3600+minutes*60+seconds
                            
                            NSLog("kkk %@", Model.sharedInstance().timesortedArray)
                            var count = 4

                            
                            var array = [Int]()
                            if (Model.sharedInstance().timesortedArray.count == 0)

                            {}
                            else
                            {
                             array = Model.sharedInstance().timesortedArray[0] as! [Int]
                                
                                for i in 0..<array.count{
                                    
                                    NSLog("i == %d", i)
                                    
                                    var reminder : Int    = array[i]
                                    
                                    reminder              = reminder * 60
                                    
                                    if !(totaltimeElapsed == 0){
                                        
                                        if (totaltimeElapsed == reminder){
                                            
                                            count = i
                                            
                                            //cancel all local notification
                                            UIApplication.sharedApplication().cancelAllLocalNotifications()
                                            
                                        }
                            }
                                
                                    if count == 0{
                                    
                                        //cancel all local notification
                                        
                                        // schedule new notification
                                        
                                        let notification         = UILocalNotification()
                                        
                                        notification.fireDate    = NSDate(timeIntervalSinceNow: 1)
                                        
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "myAlarmCategory"
                                        
                                        notification.alertBody   = "First Reminder"
                                       
                                        if #available(iOS 8.2, *) {
                                            notification.alertTitle = Model.sharedInstance().firstTone + ".aiff"
                                        } else {
                                            // Fallback on earlier versions
                                        }
                                        
                                        if Model.sharedInstance().firstTone == "Math Equation"
                                        {
                                            if #available(iOS 8.2, *) {
                                                notification.alertTitle = Model.sharedInstance().firstTone
                                            } else {
                                                // Fallback on earlier versions
                                            }
                                         notification.soundName             = Model.sharedInstance().firstTone
                                        }
                                        else{
                                            
                                            let str = Model.sharedInstance().firstTone + ".aiff"
                                            let result1 = str.containsString("Sounds")
                                            
                                            if result1  {
                                                
                                                let fileUrl =  NSURL(string: str)
                                                
                                                let strstr = fileUrl?.lastPathComponent
                                                
                                                print(strstr)
                                                
                                                notification.soundName   = strstr
                                                
                                            }
                                            else
                                            {
                                                notification.soundName   = Model.sharedInstance().firstTone + ".aiff"
                                                
                                            }

                                        }
                                        
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "shoppingListReminderCategory"
                                        
                                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                        
                                        return
                                    }
                                        
                                    else if count == 1{
                                        
                                        //cancel all local notification
//                                        UIApplication.sharedApplication().cancelAllLocalNotifications()
                                        
                                        // schedule new notification
                                        let notification         = UILocalNotification()
                                        
                                        notification.fireDate    = NSDate(timeIntervalSinceNow: 1)
                                        
                                        notification.alertBody   = "Wake Up!"
                                        
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "myAlarmCategory"
                                        
                                        notification.alertBody   = "Second Reminder"
                                        
                                        if #available(iOS 8.2, *) {
                                            notification.alertTitle = Model.sharedInstance().secondTone + ".aiff"
                                        } else {
                                            // Fallback on earlier versions
                                        }
                                        
                                        
                                        if Model.sharedInstance().secondTone == "Math Equation"
                                        {
                                            
                                            if #available(iOS 8.2, *) {
                                                notification.alertTitle = Model.sharedInstance().secondTone
                                            } else {
                                                // Fallback on earlier versions
                                            }
                                            notification.soundName           = Model.sharedInstance().secondTone
                                        }
                                        else{
                                            
                                            
                                            
                                            let str = Model.sharedInstance().secondTone + ".aiff"
                                            let result1 = str.containsString("Sounds")
                                            
                                            if result1  {
                                                
                                                let fileUrl =  NSURL(string: str)
                                                
                                                let strstr = fileUrl?.lastPathComponent
                                                
                                                print(strstr)
                                                
                                                notification.soundName   = strstr
                                                
                                            }
                                            else
                                            {
                                                notification.soundName   = Model.sharedInstance().secondTone + ".aiff"
                                                
                                            }
                                            
                                        }
                                       
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "shoppingListReminderCategory"
                                        
                                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                        
                                        return
                                    }
                                        
                                    else if count == 2{
                                        
//                                        //cancel all local notification
//                                        UIApplication.sharedApplication().cancelAllLocalNotifications()
//                                        
                                        
                                        // schedule new notification
                                        let notification         = UILocalNotification()
                                        
                                        notification.fireDate    = NSDate(timeIntervalSinceNow: 1)
                                        
                                        notification.alertBody = "Wake Up!"
                                        
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "myAlarmCategory"
                                        
                                        notification.alertBody   = "Third Reminder"
                                        
                                        if #available(iOS 8.2, *) {
                                            notification.alertTitle = Model.sharedInstance().thirdTone + ".aiff"
                                        } else {
                                            // Fallback on earlier versions
                                        }
                                        
                                        if Model.sharedInstance().thirdTone == "Math Equation"
                                        {
                                            if #available(iOS 8.2, *) {
                                                notification.alertTitle = Model.sharedInstance().thirdTone
                                            } else {
                                                // Fallback on earlier versions
                                            }
                                            notification.soundName   = Model.sharedInstance().thirdTone
                                        }
                                        else{
                                            
                                            let str = Model.sharedInstance().thirdTone + ".aiff"
                                            let result1 = str.containsString("Sounds")
                                            
                                            if result1  {
                                                
                                                let fileUrl =  NSURL(string: str)
                                                
                                                let strstr = fileUrl?.lastPathComponent
                                                
                                                print(strstr)
                                                
                                                notification.soundName   = strstr
                                            }
                                            else
                                            {
                                                notification.soundName   = Model.sharedInstance().thirdTone + ".aiff"
                                            }
                                        }
                                        
                                        notification.alertAction = "Open App"
                                        
                                        notification.category    = "shoppingListReminderCategory"
                                        
                                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                        
                                       return
                                    }
                                    
                                }  // end of 1st timeElapsed check
                                
                            }  // end of 2nd timeElapsed check

                      } // end of reminder set
                        
                }  // end of total time check
                else{

                       let formatter: NSDateFormatter = NSDateFormatter()
                    
                        formatter.dateFormat = "yyyy-MM-dd"
                    
                        alarmInfo.date       = formatter.stringFromDate(NSDate())
                    
                        var time             = Model.sharedInstance().totalTimeForUsage
                        
                        Model.sharedInstance().totalTimeForUsage = "00:00"
                    
                        time                 = time.stringByReplacingOccurrencesOfString(":", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                        alarmInfo.Consumed   = time
                        
                        // update today's iphone usage in data base for record.
                        let isInserted = ModelManager.getInstance().updateConsumedTime(alarmInfo)
                    
                        print("data is inserted:-",isInserted)

                        //cancel all local notification
                        UIApplication.sharedApplication().cancelAllLocalNotifications()
                        
                        // schedule new notification
                        let notification         = UILocalNotification()
                    
                        notification.fireDate    = NSDate(timeIntervalSinceNow: 1)
                    
                        notification.alertBody   = "Your time for today iPhone usage has competed"
                        
                        if #available(iOS 8.2, *) {
                         notification.alertTitle = Model.sharedInstance().normalAlarmRingTone + ".aiff"
                        } else {
                            // Fallback on earlier versions
                        }
                        
                        if Model.sharedInstance().normalAlarmRingTone == "Math Equation"{
                            
                            if #available(iOS 8.2, *) {
                                notification.alertTitle = Model.sharedInstance().normalAlarmRingTone
                            } else {
                                // Fallback on earlier versions
                            }
                            
                            notification.soundName   = Model.sharedInstance().normalAlarmRingTone
                        }
                        else{

                            let str = Model.sharedInstance().normalAlarmRingTone + ".aiff"
                            let result1 = str.containsString("Sounds")
                            
                            if result1  {
                                
                                let fileUrl =  NSURL(string: str)
                                
                                let strstr = fileUrl?.lastPathComponent
                                
                                print(strstr)
                                
                                notification.soundName   = strstr
                                
                            }
                            else
                            {
                                notification.soundName   = Model.sharedInstance().normalAlarmRingTone + ".aiff"

                            }
                            
                        }
                        
                        NSLog(Model.sharedInstance().normalAlarmRingTone)
                        
                        notification.alertAction = "Open App"
                    
                        notification.category    = "shoppingListReminderCategory"
                        
                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    
                        Model.sharedInstance().FirstNotification = true
                        
                        // set final image on progress view
                        let image1 = UIImage(named: "clockred.png")
                        
                        self.clockInnerCircle_imgVew.image       = image1
                        
                        self.chronometer.reset()
                        
                        Model.sharedInstance().totalTimeBool     = true
                        
                        Model.sharedInstance().alarmSet          = false
                        
                    }
                        self.circleProgressView.progress         = Double(appDelegate.totaltime)
                
                } // end of progress view else
                
           } // end of chronometer
            
           chronometer.start()
            
       } // end of total time usage check
        
    }  // end of view did appear
    
    
    // Following Button is used for moving on the setting screen of the app
    @IBAction func settingBtnAction(sender: AnyObject) {
        
        let settingViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SettingViewController") as! SettingViewController
        self.navigationController?.pushViewController(settingViewController, animated: true)
   
    }

     override func didReceiveMemoryWarning()
     {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     }
    
    // Register local notification and add actions
    func setupNotificationSettings() {
        
        let notificationSettings: UIUserNotificationSettings! = UIApplication.sharedApplication().currentUserNotificationSettings()
        
        if (notificationSettings.types == UIUserNotificationType.None){
            
            // Specify the notification types.
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound]
            
            // Specify the notification actions.
            
            let modifyListAction                    = UIMutableUserNotificationAction()
            modifyListAction.identifier             = "EnableReminder"
            modifyListAction.title                  = "Enable Reminder"
            modifyListAction.activationMode         = UIUserNotificationActivationMode.Foreground
            modifyListAction.destructive            = false
            modifyListAction.authenticationRequired = true
            
            let actionsArray = NSArray(objects:  modifyListAction)
            let actionsArrayMinimal = NSArray(objects:  modifyListAction)
            
            // Specify the category related to the above actions.
            
            let shoppingListReminderCategory        = UIMutableUserNotificationCategory()
            shoppingListReminderCategory.identifier = "shoppingListReminderCategory"
            shoppingListReminderCategory.setActions(actionsArray as? [UIUserNotificationAction], forContext: UIUserNotificationActionContext.Default)
            shoppingListReminderCategory.setActions(actionsArrayMinimal as? [UIUserNotificationAction], forContext: UIUserNotificationActionContext.Minimal)
            let categoriesForSettings = NSSet(objects: shoppingListReminderCategory)
            
            // Register the notification settings.
            let newNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
            UIApplication.sharedApplication().registerUserNotificationSettings(newNotificationSettings)
            
        }
    }
    
    // MARK: Custom method implementation
    func configureScrollView() {
        // Enable paging.
        scrollView.pagingEnabled = true
        
        // Set the following flag values.
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.scrollsToTop = true
        
        // Set the scrollview content size.
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * CGFloat(totalPages), scrollView.frame.size.height-20)
        
        // Set self as the delegate of the scrollview.
        scrollView.delegate = self
        
        // Load the TestView view from the TestView.xib file and configure it properly.
        
        for i in 0 ..< totalPages {
            
            // Load the view.
            
            if i == 0 {
                self.progressviewBackgroundView.frame = CGRectMake(CGFloat(i) * scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)

                scrollView.addSubview(self.progressviewBackgroundView)
            }
            else
            {
                
                self.barchartViewBg.frame = CGRectMake(CGFloat(i) * scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)
                
                scrollView.addSubview(self.barchartViewBg)

            }
        }
    }
    
    func configurePageControl() {
        // Set the total pages to the page control.
        pageControl.numberOfPages = totalPages
        
        // Set the initial page.
        pageControl.currentPage = 0
    }
    
    // MARK: UIScrollViewDelegate method implementation
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        // Calculate the new page index depending on the content offset.
        let currentPage = floor(scrollView.contentOffset.x / UIScreen.mainScreen().bounds.size.width);
        
        // Set the new page index to the page control.
        pageControl.currentPage = Int(currentPage)
        
    }
    
    @IBAction func changePage(sender: AnyObject) {
        
        // Calculate the frame that should scroll to based on the page control current page.
        var newFrame = scrollView.frame
        newFrame.origin.x = newFrame.size.width * CGFloat(pageControl.currentPage)
        scrollView.scrollRectToVisible(newFrame, animated: true)
        
    }
    
    // bar chart functionality
    // MARK: - Table view data source
    // MARK: Section Info
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        
        NSLog("dataRetrived:- %@",dataRetrived)
        if !dataRetrived
        {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if !dataRetrived {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:BarChartTableviewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! BarChartTableviewCell
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.x, cell.frame.width, tableView.frame.height)
        
        cell.configUI(indexPath)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        print(tableView.frame.height)
        return tableView.frame.height;
    }
    

    // Method for allocating sound session
    func setSessionPlayback() {
        
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        
        do {
            try! session.setCategory(AVAudioSessionCategoryPlayback)
        }
        do {
            try! session.setActive(true)
        }

    }

    //math equation view
    @IBAction func okClicked(sender: AnyObject) {
        
        let webView = UIWebView()
        let result = webView.stringByEvaluatingJavaScriptFromString((randomExpression_lbl?.text)!)!
        
        if mathEquationResult_txt.text == result {
            
            mathEquation_Vew.hidden = true
            visualEffectView.hidden = true
            mathEquationResult_txt.resignFirstResponder()

        }
        else{
            let alert : UIAlertView = UIAlertView()
            alert.title           = "Sorry!"
            alert.message             = "Your answer is not correct,Please enter a correct answer"
            alert.addButtonWithTitle("Ok")
            alert.show()
        }
        
        print("result\(result)")

    }
    
    // Returns an array of unique random numbers with range
    
    func getRandomNumbersOfSize(size: Int, from lowerLimit: Int, to higherLimit: Int) -> [NSMutableArray] {
        let uniqueNumbers = NSMutableArray()
        var r: Int
        
        while uniqueNumbers.count < size {
            
            let max = UInt32(higherLimit)
            let min = UInt32(lowerLimit)
            
            r = Int(min + arc4random_uniform(max - min + 1))
            
            if !uniqueNumbers.containsObject(Int(r)) {
                uniqueNumbers.addObject(Int(r))
            }
        }
        return [uniqueNumbers]
    }

    // code for getting rendom expression
    func getrandomExpression() -> String {
        
        var rndValue = 100 + arc4random() % (500 - 100)
        var expression = ""
        var n : NSInteger = 0
        var numberarray = [AnyObject]()
        
        while rndValue > 0 {
            n = Int(rndValue % 10)
            rndValue /= 10
            print("\(n)")
            let myString = "\(n)"
            numberarray.append(myString)
        }
        
        var operators = ["+", "-", "*"]
        var randomOperatorIndexArray = self.getRandomNumbersOfSize(3, from: 0, to: (operators.count - 1))
        expression = numberarray[2] as! String
        for j in 0..<3 {
            expression = "\(expression)\(numberarray[j])"
            let i = randomOperatorIndexArray[0]
            let index = i[j]
            print("print iiii",i)
            let randomOperator = operators[Int(index as! NSNumber)]
            expression = "\(expression)\(randomOperator)"
            print("\(randomOperator)")
            print("\(expression)")
            if j == 2 {
                expression = "\(expression)\(numberarray[0])"
                print("\(expression)")
            }
        }
        
        return expression
    }
    
}