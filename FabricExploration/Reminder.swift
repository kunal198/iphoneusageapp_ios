//
//  Reminder.swift
//  ScreenUsages
//
//  Created by brst on 8/16/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit

class Reminder: UIViewController {
    
    var array1 = [Int]()
    
    @IBOutlet weak var secondTimeLbl: UILabel!
    @IBOutlet weak var selectFirstTimeLbl: UILabel!
    @IBOutlet weak var selectThirdTimeLbl: UILabel!
    @IBOutlet weak var selectFirstToneLbl: UILabel!
    
    @IBOutlet weak var selectThirdToneLbl: UILabel!
    @IBOutlet weak var selectSecondToneLbl: UILabel!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBOutlet weak var BorderView: UIView!
    @IBOutlet weak var firstlineView: UIView!
    @IBOutlet weak var secondlineView: UIView!
    @IBOutlet weak var alarmTypeBackgroundView: UIView!
    
    @IBOutlet weak var doneOrCancelBtnBackgroundView: UIView!
    @IBOutlet weak var reminderPicker: UIPickerView!
    @IBOutlet weak var pickerBackgroundView: UIView!
    @IBOutlet weak var segmentedControlBorderView: UIView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var timerBorderView: UIView!
    @IBOutlet weak var selectFirstToneBorderView: UIView!
    @IBOutlet weak var selectSecondToneBorderView: UIView!
    
    @IBOutlet weak var selectThirdToneBorderView: UIView!
    @IBOutlet weak var selectFirstTimeBorderView: UIView!
    @IBOutlet weak var selectSecondTimeBorderView: UIView!
    @IBOutlet weak var selectThirdTimeBorderView: UIView!
    
    var selectToneButton :NSString      = ""
    var selectTimeForReminder :NSString = ""
    var selectedTimeForTimer : String   = ""
    

    // Time Array
    let timeArray  = ["1","2","3","5","10","15","20","25","30","40","50","60"]

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // set Reminder Tone
        
        if Model.sharedInstance().firstTone  != "" {
            
            self.selectFirstToneLbl?.text     = Model.sharedInstance().firstTone as String
        }
        if Model.sharedInstance().secondTone != "" {
            self.selectSecondToneLbl?.text    = Model.sharedInstance().secondTone as String
        }
        if Model.sharedInstance().thirdTone  != "" {
            self.selectThirdToneLbl?.text     = Model.sharedInstance().thirdTone as String
        }

        //set reminder time
        if  Model.sharedInstance().firstTime != ""{
            self.selectFirstTimeLbl?.text     =  NSString(format: "%@ min", Model.sharedInstance().firstTime) as String
        }
        
        if Model.sharedInstance().secondTime != "" {
            self.secondTimeLbl?.text          = NSString(format: "%@ min", Model.sharedInstance().secondTime) as String
        }
        
        if Model.sharedInstance().thirdTime  != "" {
            self.selectThirdTimeLbl?.text     = NSString(format: "%@ min",Model.sharedInstance().thirdTime) as String
        }
        
        // hide and show timer border view accordingly
        if !(Model.sharedInstance().timeBorderViewHidden) {
            self.timerBorderView.hidden                      = false
            
            if Model.sharedInstance().timerborderViewHeight  == "one" {
                self.timerBorderView.frame.size.height       = 81
                self.firstlineView.hidden                    = true
                self.BorderView.frame.size.height            = 60
                self.segmentedControl.selectedSegmentIndex   = 1;
            }
            else if Model.sharedInstance().timerborderViewHeight == "two"{
                self.timerBorderView.frame.size.height       = 141
                self.BorderView.frame.size.height            = 121
                self.firstlineView.hidden                    = false
                self.secondlineView.hidden                   = true
                self.segmentedControl.selectedSegmentIndex   = 2
            }
            else{
                self.timerBorderView.frame.size.height       = 203
                self.BorderView.frame.size.height            = 183
                self.firstlineView.hidden                    = false
                self.secondlineView.hidden                   = false
                self.segmentedControl.selectedSegmentIndex   = 3
            }
        }
        
    // following lines of code is used for making respected view's round corner and their borderwidth and border color
        
        self.segmentedControlBorderView.layer.cornerRadius    = 10
        self.segmentedControlBorderView.clipsToBounds         = true
        
        self.timerBorderView.layer.cornerRadius               = 10
        self.timerBorderView.clipsToBounds                    = true
        
        self.alarmTypeBackgroundView.layer.cornerRadius       = 10
        self.alarmTypeBackgroundView.clipsToBounds            = true
        
        self.BorderView.layer.borderColor                     = anyColor().CGColor
        self.BorderView.layer.borderWidth                     = 1
        
        self.selectFirstToneBorderView.layer.borderColor      = anyColor().CGColor
        self.selectFirstToneBorderView.layer.borderWidth      = 1
        
        self.selectSecondToneBorderView.layer.borderColor     = anyColor().CGColor
        self.selectSecondToneBorderView.layer.borderWidth     = 1
        
        self.selectThirdToneBorderView.layer.borderColor      = anyColor().CGColor
        self.selectThirdToneBorderView.layer.borderWidth      = 1
        
        self.selectFirstTimeBorderView.layer.borderColor      = anyColor().CGColor
        self.selectFirstTimeBorderView.layer.borderWidth      = 1
        
        self.selectSecondTimeBorderView.layer.borderColor     = anyColor().CGColor
        self.selectSecondTimeBorderView.layer.borderWidth     = 1
        
        self.selectThirdTimeBorderView.layer.borderColor      = anyColor().CGColor
        self.selectThirdTimeBorderView.layer.borderWidth      = 1
        
        self.doneOrCancelBtnBackgroundView.layer.cornerRadius = 3
        self.doneOrCancelBtnBackgroundView.clipsToBounds      = true
        
        self.alarmTypeBackgroundView.layer.cornerRadius       = 10
        self.alarmTypeBackgroundView.clipsToBounds            = true
        
        self.pickerBackgroundView.layer.cornerRadius          = 10
        self.pickerBackgroundView.clipsToBounds               = true
        
        self.reminderPicker.layer.cornerRadius                = 10
        self.reminderPicker.clipsToBounds                     = true
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // set Reminder Tone
        
        if Model.sharedInstance().firstTone  != "" {
            self.selectFirstToneLbl?.text     = Model.sharedInstance().firstTone as String
        }
        if Model.sharedInstance().secondTone != "" {
            self.selectSecondToneLbl?.text    = Model.sharedInstance().secondTone as String
        }
        if Model.sharedInstance().thirdTone  != "" {
            self.selectThirdToneLbl?.text     = Model.sharedInstance().thirdTone as String
        }

     }

    // set color of border
    func anyColor() -> UIColor {
        return UIColor(red: 21.0/255.0, green: 126.0/255.0, blue: 247.0/255.0, alpha: 1.0)
    }
    
    // Done or cancel Button action
    @IBAction func doneOrCancelBtnAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = true
        self.alarmTypeBackgroundView?.hidden = true
        
    }
    
    //Pragma Mark
    //segmented Control Action
    
    @IBAction func segmentControlAction(sender: AnyObject) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            
            Model.sharedInstance().timesortedArray = NSMutableArray()
            
            
            //make all reminder false
            Model.sharedInstance().firstreminder  = false
            Model.sharedInstance().secondReminder = false
            Model.sharedInstance().thirdReminder  = false
            Model.sharedInstance().firstTone      = "Select Tone"
            Model.sharedInstance().secondTone     = "Select Tone"
            Model.sharedInstance().thirdTone      = "Select Tone"
            Model.sharedInstance().firstTime      = "Select Time"
            Model.sharedInstance().secondTime     = "Select Time"
            Model.sharedInstance().thirdTime      = "Select Time"
            
            self.selectFirstToneLbl?.text  = Model.sharedInstance().firstTone as String
            
            self.selectSecondToneLbl?.text = Model.sharedInstance().secondTone as String
            
            self.selectThirdToneLbl?.text  = Model.sharedInstance().thirdTone as String
            
            self.selectFirstTimeLbl?.text  = Model.sharedInstance().firstTime as String
            
            self.secondTimeLbl?.text       = Model.sharedInstance().secondTime as String
            
            self.selectThirdTimeLbl?.text  = Model.sharedInstance().thirdTime as String
            
            self.timerBorderView.hidden                  = true
            Model.sharedInstance().timeBorderViewHidden  = true

        case 1:
            self.timerBorderView.hidden                  = false
            self.timerBorderView.frame.size.height       = 81
            self.firstlineView.hidden                    = true
            self.BorderView.frame.size.height            = 60
            Model.sharedInstance().timeBorderViewHidden  = false
            Model.sharedInstance().timerborderViewHeight = "one"

        case 2:
            self.timerBorderView.hidden                  = false
            self.timerBorderView.frame.size.height       = 141
            self.BorderView.frame.size.height            = 121
            self.firstlineView.hidden                    = false
            self.secondlineView.hidden                   = true
            Model.sharedInstance().timeBorderViewHidden  = false
            Model.sharedInstance().timerborderViewHeight = "two"

        case 3:
            self.timerBorderView.hidden                  = false
            self.timerBorderView.frame.size.height       = 203
            self.BorderView.frame.size.height            = 183
            self.firstlineView.hidden                    = false
            self.secondlineView.hidden                   = false
            Model.sharedInstance().timeBorderViewHidden  = false
            Model.sharedInstance().timerborderViewHeight = "three"

        default:
            break;
        }
    }
    
    // Pragma Mark
    //Picker Done Button Action
    
    @IBAction func pickerDoneBtnAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden     = true
        self.pickerBackgroundView?.hidden = true
        
    }
    
    // Set Math equation on and off
    @IBAction func mathEquationOnOffBtnAction(sender: AnyObject) {
        
        if switchBtn.on == false{
            
            //save tone for showing on labels
            if self.selectToneButton == "FirstToneButton" {
                
                Model.sharedInstance().firstTone            = ""
                self.selectFirstToneLbl?.text               = "Select Tone"
                Model.sharedInstance().firstreminder        = true
                Model.sharedInstance().firstRingToneType    = ""
                
            }
            else if self.selectToneButton == "SecondToneButton"{
                
                Model.sharedInstance().secondReminder       = true
                Model.sharedInstance().secondRingToneType   = ""
                Model.sharedInstance().secondTone           = ""
                self.selectSecondToneLbl?.text              = "Select Tone"
                
            }
            else if self.selectToneButton == "ThirdToneButton"{
                
                Model.sharedInstance().thirdReminder        = true
                Model.sharedInstance().thirdRinngToneType   = ""
                Model.sharedInstance().thirdTone            = ""
                self.selectThirdToneLbl?.text               = "Select Tone"
                
            }

        }
        
        if switchBtn.on == true{
            //save tone for showing on labels
            if self.selectToneButton                         == "FirstToneButton"
            {
                Model.sharedInstance().firstTone             = "Math Equation"
                self.selectFirstToneLbl?.text                = "Math Equation"
                Model.sharedInstance().firstreminder         = true
                Model.sharedInstance().firstRingToneType     = "Math Equation"
            }
            else if self.selectToneButton                    == "SecondToneButton"
            {
                Model.sharedInstance().secondTone            = "Math Equation"
                self.selectSecondToneLbl?.text               = "Math Equation"
                Model.sharedInstance().secondReminder        = true
                Model.sharedInstance().secondRingToneType    = "Math Equation"
            }
            else if self.selectToneButton                    == "ThirdToneButton"{
                Model.sharedInstance().thirdTone             = "Math Equation"
                self.selectThirdToneLbl?.text                = "Math Equation"
                Model.sharedInstance().thirdReminder         = true
                Model.sharedInstance().thirdRinngToneType    = "Math Equation"
            }
    
        }

    }
    
    // Following button action is used for setting Alarm Type.It could be a ringtone, voice Recording
    @IBAction func alarmTypeButtonAction(sender: AnyObject) {
       
        let buttonTag = sender.tag
        
        if buttonTag == 0 {
            
            let ringtoneViewController             = self.storyboard?.instantiateViewControllerWithIdentifier("RingtoneViewController") as! RingtoneViewController
            ringtoneViewController.selectedToneFor = self.selectToneButton
            self.navigationController?.pushViewController(ringtoneViewController, animated: true)
            
        }
        else{
            
            let recordingViewcontroller             = self.storyboard?.instantiateViewControllerWithIdentifier("RecordingViewcontroller") as!RecordingViewcontroller
            recordingViewcontroller.selectedToneFor = self.selectToneButton
            self.navigationController?.pushViewController(recordingViewcontroller, animated: true)

        }
        
        // make visual effect view and alarm's type background view hidden
        self.visualEffectView?.hidden               = true
        self.alarmTypeBackgroundView?.hidden        = true
        
    }
    
    // Follwing Button action is used for selecting your alarm type for first Reminder
    @IBAction func selectFstToneAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = false
        self.selectToneButton                = "FirstToneButton"
        if Model.sharedInstance().firstTone  == "Math Equation"
        {
            self.switchBtn.on = true
        }
        else
        {
            self.switchBtn.on = false
        }
        
    }
    
    // Follwing Button action is used for selecting your alarm type for Second Reminder
    @IBAction func selectSecondBtnAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = false
        self.selectToneButton                = "SecondToneButton"
        if Model.sharedInstance().secondTone == "Math Equation"
        {
            self.switchBtn.on = true
        }
        else
        {
            self.switchBtn.on = false
        }
        
    }
    
    // Follwing Button action is used for selecting your alarm type for Third Reminder
    @IBAction func selectThrdBtnAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = false
        self.selectToneButton                = "ThirdToneButton"
        if Model.sharedInstance().thirdTone  == "Math Equation"
        {
            self.switchBtn.on = true
        }
        else
        {
            self.switchBtn.on = false
        }
        
    }
    
    // Follwing Button action is used for selecting First Reminder Time
    @IBAction func selectTimeForFirstReminderAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = true
        self.pickerBackgroundView?.hidden    = false
        self.selectTimeForReminder           = "TimeForFirstTimerSelected"
        
    }
    
    // Follwing Button action is used for selecting Second Reminder Time
    @IBAction func selectTimeForSecondReminderAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = true
        self.pickerBackgroundView?.hidden    = false
        self.selectTimeForReminder           = "TimeForSecondTimerSelected"
        
    }

    // Follwing Button action is used for selecting Third Reminder Time
    @IBAction func selectTimeForThirdReminderAction(sender: AnyObject) {
        
        self.visualEffectView?.hidden        = false
        self.alarmTypeBackgroundView?.hidden = true
        self.pickerBackgroundView?.hidden    = false
        self.selectTimeForReminder           = "TimeForThirdTimerSelected"
        
    }
  
    //Following Button is used for moving on last Screen
    @IBAction func backButtonAction(sender: AnyObject) {
        
        let array = array1
        if array1.count != 0 {
        
            for  _ in 0..<array.count
            {
                if array1[0] == 0
                {
                    array1.removeAtIndex(0)
                   
                }
            }
            
            Model.sharedInstance().timesortedArray[0] = array1
            print(array1)
            print(Model.sharedInstance().timesortedArray[0])
            
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // pragma MARK :- PickerView DataSource and Delegate
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return timeArray.count;
        }
        else{
            return 1
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0
        {
            return timeArray[row]
        }
        else
        {
            return "min"
        }
    }
    
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        
        if component == 0 {
            return 45
        }
        else
        {
            return 50
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let time = ["1","2","3","5","10","15","20","25","30","40","50","60"]
        
        // set time for reminders
        if self.selectTimeForReminder == "TimeForFirstTimerSelected"{
            
            Model.sharedInstance().firstTime             = time[row]
            NSLog(Model.sharedInstance().firstTime)
            self.selectFirstTimeLbl?.text                = NSString(format: "%@  min", time[row]) as String
            
        }
        else if self.selectTimeForReminder == "TimeForSecondTimerSelected"{
            
            Model.sharedInstance().secondTime            = time[row]
            self.secondTimeLbl?.text                     = NSString(format: "%@ min", time[row]) as String
            
        }
        else if self.selectTimeForReminder == "TimeForThirdTimerSelected"{

            Model.sharedInstance().thirdTime             = time[row]
            self.selectThirdTimeLbl?.text                = NSString(format: "%@ min", time[row]) as String
            
        }
        
        let dict = ["firstTime": Model.sharedInstance().firstTime, "secondTime": Model.sharedInstance().secondTime, "thirdTime": Model.sharedInstance().thirdTime ]

        Model.sharedInstance().timesortedArray = NSMutableArray()
        
        Model.sharedInstance().timesortedArray.addObject(dict)
        
        let dictValue : Int  = (Model.sharedInstance().timesortedArray[0].valueForKey("firstTime"))!.integerValue
        
        Model.sharedInstance().timesortedArray.addObject(dictValue)
        
        let dictValue1 : Int = (Model.sharedInstance().timesortedArray[0].valueForKey("secondTime"))!.integerValue
        
        Model.sharedInstance().timesortedArray.addObject(dictValue)
        
        let dictValue2       = (Model.sharedInstance().timesortedArray[0].valueForKey("thirdTime"))!.integerValue
        
        Model.sharedInstance().timesortedArray.addObject(dictValue)
        
        array1               = [dictValue, dictValue1, dictValue2]
        
        array1.sortInPlace( { $0 < $1 })
        
        Model.sharedInstance().timesortedArray = NSMutableArray()
        
        Model.sharedInstance().timesortedArray.addObject(array1)
        
        array1 = Model.sharedInstance().timesortedArray[0] as! [Int]
        
        Model.sharedInstance().reminderSet = true

    }
}