//
//  PhoneLockViewController.swift
//  ScreenUsages
//
//  Created by brst on 9/21/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import UIKit

class PhoneLockViewController: UIViewController {
    
    var count = 0
    var passwordString = ""
    var confirmPasswordString = ""
    var star = ""
    var removePassword = false
    var canSetAlarm = false
    
    
    
    @IBOutlet weak var passwordScreenTitle_lbl: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star6: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star7: UIImageView!
    @IBOutlet weak var star8: UIImageView!
    @IBOutlet weak var star9: UIImageView!
    @IBOutlet weak var enterPassword_vew: UIView!
  
    // viewDid Load
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults      =  NSUserDefaults .standardUserDefaults()
        let passwordSaved = defaults .boolForKey("passwordSaved")
        
        if passwordSaved == true {
            passwordScreenTitle_lbl.text = "Enter Your Pin"
        }
        else{
            passwordScreenTitle_lbl.text = "Choose Your Password"
        }
    }
    
    // clear button action
    @IBAction func clearBTnAction(sender: AnyObject) {
        
        if count != 0 {
            
            let i = count
            
            if i == 1 {
                star1.image = nil
            }
            else if i == 2{
                star2.image = nil
            }
            else if i == 3{
                star3.image = nil
            }
            else if i == 4{
                star4.image = nil
            }
            else if i == 5{
                star5.image = nil
            }
            else if i == 6{
                star6.image = nil
            }
            else if i == 7{
                star7.image = nil
            }
            else if i == 8{
                star8.image = nil
            }
            else{
                star9.image = nil
            }
            if passwordScreenTitle_lbl.text == "Choose Your Password" || passwordScreenTitle_lbl.text == "Enter Your Pin" {
                
                 passwordString = String(passwordString.characters.dropLast())

            }
            else
            {
                
                confirmPasswordString = String(passwordString.characters.dropLast())
                
            }
            count -= 1
        }
        
    }
    
    // key pressed
    @IBAction func keyBtnAction(sender: AnyObject) {
    
        count += 1
        let  i = sender.tag
        
        if count > 9
        {
            
            let alert : UIAlertView = UIAlertView()
            alert.title = "Sorry!"
            alert.message = "You can not Exceed the limit"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        else{
            
            if  passwordScreenTitle_lbl.text == "Choose Your Password"{
                passwordString = passwordString + String(i)
            }
            else if passwordScreenTitle_lbl.text == "Confirm Your Password"{
                confirmPasswordString = confirmPasswordString + String(i)
            }
            else if passwordScreenTitle_lbl.text == "Enter Your Pin"{
                passwordString = passwordString + String(i)
            }
        }

        for i in 1...count
        {
            if i == 1 {
                star1.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 2{
                star2.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 3{
                star3.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 4{
                star4.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 5{
                star5.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 6{
                star6.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 7{
                star7.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else if i == 8{
                star8.image = UIImage (imageLiteral: "Asterisco.png")
            }
            else{
                star9.image =  UIImage (imageLiteral: "Asterisco.png")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    @IBAction func okBtn_Action(sender: AnyObject) {
        
        if passwordString == "" {
            let alert : UIAlertView = UIAlertView()
            alert.message = "Please enter password"
            alert.addButtonWithTitle("Ok")
            alert.show()
        }else{
        
            if passwordScreenTitle_lbl.text == "Confirm Your Password"{
                if passwordString == confirmPasswordString{
                    
                    count = 0
                    let alert : UIAlertView = UIAlertView()
                    alert.title = "Your Passward has been saved successfully"
                    alert.addButtonWithTitle("OK")
                    alert.delegate = self
                    alert.tag = 0
                    alert.show()
                    
                }
                else
                {
                    let alert : UIAlertView = UIAlertView()
                    alert.title = "Password Didn't match.Please Try Again"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                    passwordScreenTitle_lbl.text = "Choose Your Password"
                    passwordString = ""
                    confirmPasswordString = ""
                    star1.image = nil
                    star2.image = nil
                    star3.image = nil
                    star4.image = nil
                    star5.image = nil
                    star6.image = nil
                    star7.image = nil
                    star8.image = nil
                    star9.image = nil
                    count = 0
                }
            }
            else if passwordScreenTitle_lbl.text == "Choose Your Password" {
                
                count = 0
                passwordScreenTitle_lbl.text = "Confirm Your Password"
                star1.image = nil
                star2.image = nil
                star3.image = nil
                star4.image = nil
                star5.image = nil
                star6.image = nil
                star7.image = nil
                star8.image = nil
                star9.image = nil
                
            }
            else if passwordScreenTitle_lbl.text == "Enter Your Pin"{
                
                count = 0
                let defaults = NSUserDefaults.standardUserDefaults()
                let savedPassword = defaults.objectForKey("Password") as! String
                
                if savedPassword == passwordString {
                    if removePassword == true {
                        defaults.setBool(false, forKey: "passwordSaved")
                        Model.sharedInstance().passwordSaved = false
                        Model.sharedInstance().passwordUnlock = false
                        
                        let alert : UIAlertView = UIAlertView()
                        alert.title = "Your password has deleted successfully"
                        alert.addButtonWithTitle("OK")
                        alert.show()
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    else if canSetAlarm == true{
                        
                        
                        let reminder = self.storyboard?.instantiateViewControllerWithIdentifier("AlarmViewController") as!AlarmViewController
                        self.navigationController?.pushViewController(reminder, animated: true)
                    }
                        
                    else
                    {
                        let passwordSaved = defaults .boolForKey("passwordSaved")
                        if passwordSaved == true {
                            passwordScreenTitle_lbl.text = "Choose Your Password"
                            passwordString = ""
                            count = 0
                            star1.image = nil
                            star2.image = nil
                            star3.image = nil
                            star4.image = nil
                            star5.image = nil
                            star6.image = nil
                            star7.image = nil
                            star8.image = nil
                            star9.image = nil
                        }
                        else{
                            Model.sharedInstance().passwordUnlock = true
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                    }
                }
                else{
                    
                    star1.image = nil
                    star2.image = nil
                    star3.image = nil
                    star4.image = nil
                    star5.image = nil
                    star6.image = nil
                    star7.image = nil
                    star8.image = nil
                    star9.image = nil
                    let alert : UIAlertView = UIAlertView()
                    alert.title = "Sorry!"
                    alert.message = "You Entered Wrong Password"
                    alert.addButtonWithTitle("Ok")
                    alert.show()
                }
                passwordString = ""
            }
        }
    }
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int) {
        
        switch View.tag {
        case 0:
            count = 0
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults .setObject(passwordString, forKey: "Password")
            defaults.setBool(true, forKey: "passwordSaved")
            Model.sharedInstance().passwordSaved = true
            
            self.navigationController?.popViewControllerAnimated(true)

            return
        default:
            print("alertView \(buttonIndex) clicked")
        }
        passwordString = ""
    }
    
    @IBAction func backButtonAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)

    }
}
