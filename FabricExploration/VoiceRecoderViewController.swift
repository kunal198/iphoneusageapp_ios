//
//  VoiceRecoderViewController.swift
//  ScreenUsages
//
//  Created by brst on 8/19/16.
//  Copyright © 2016 Innofied. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class VoiceRecoderViewController: UIViewController {
    
    ///Model singleton
    let model = Model.sharedInstance()

    @IBOutlet weak var recordingImageView: UIImageView!
    var recorder: AVAudioRecorder!
    
    var player:AVAudioPlayer!
    
    @IBOutlet weak var stop_Recording_imgVew: UIImageView!
    @IBOutlet weak var showRecordingBtn: UIButton!
    @IBOutlet weak var startrecord_lbl: UILabel!
    @IBOutlet weak var recordingAndStop_lbl: UILabel!
    @IBOutlet weak var startAndStop_Backgroundview: UIView!
    @IBOutlet weak var recordingBackGroundView: UIView!
    @IBOutlet weak var showRemoveBackGroundView: UIView!
    @IBOutlet var recordButton: UIButton!
    
    @IBOutlet weak var recording_imgVew: UIImageView!
    
    @IBOutlet var stopButton: UIButton!
    
    @IBOutlet var statusLabel: UILabel!
    
    var meterTimer:NSTimer!
    
    var soundFileURL:NSURL!
    
    //Back button action
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    @IBAction func showRecordings(sender: AnyObject) {
        let recordingViewcontroller = self.storyboard?.instantiateViewControllerWithIdentifier("RecordingViewcontroller") as!RecordingViewcontroller
        self.navigationController?.pushViewController(recordingViewcontroller, animated: true)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        recordingBackGroundView.clipsToBounds = true
        recordingBackGroundView.layer.cornerRadius = 10.0
        
        startAndStop_Backgroundview.clipsToBounds = true
        startAndStop_Backgroundview.layer.cornerRadius = 10.0
        
        stopButton.enabled = false
        setSessionPlayback()
        askForNotifications()
        checkHeadphones()
        
    }
    
    func updateAudioMeter(timer:NSTimer) {
        
        if recorder.recording {
            
            let min = Int(recorder.currentTime / 60)
            let sec = Int(recorder.currentTime % 60)
            let s = String(format: "%02d:%02d", min, sec)
            statusLabel.text = s
            recorder.updateMeters()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        recorder = nil
        player = nil
    }

    // Remove all recordings
    @IBAction func removeAll(sender: AnyObject) {
        
        let alertView :UIAlertView = UIAlertView()
        alertView.delegate = self
        alertView.title = ""
        alertView.message = "Do you want to delete all recordings?"
        alertView.addButtonWithTitle("Ok")
        alertView.addButtonWithTitle("Cancel")
        alertView.tag = 0
        alertView.show()
        
       // deleteAllRecordings()
    }
    
    
    // alertview action
    // alert View action method
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
        switch buttonIndex{
            
        case 0:
            deleteAllRecordings()
            NSLog("stop default")
            break
        case 1:
             break
                default:
            NSLog("Default")
            break
            
        }
    }
    
    
    @IBAction func record(sender: UIButton) {
       // rec-big-dis-icon0022
        showRecordingBtn.hidden = true
        stop_Recording_imgVew.image = UIImage (imageLiteral: "stop.png")
        recording_imgVew.image = UIImage (imageLiteral: "rec.png")
        
        if  player != nil && player.playing {
            player.stop()
        }
        
        print(recordButton.titleLabel?.text)
        
        if  recordButton.titleLabel?.text == "Record" {
            recorder = nil
        }
        
        if  recorder == nil {
            recordingImageView.image = UIImage (imageLiteral: "rec-big-dis-icon0022")
            print("recording. recorder nil")
            recordButton.setTitle("Pause", forState:.Normal)
            stopButton.enabled = true
            recordWithPermission(true)
            return
        }
        
        if  recorder != nil && recorder.recording {
            print("pausing")
            
            recordingImageView.image = UIImage (imageLiteral: "pause.png")
            recorder.pause()
            recordButton.setTitle("Continue", forState:.Normal)
            
        }   else {
            print("recording")
            
            recordingImageView.image = UIImage (imageLiteral: "rec-big-dis-icon0022")
            recordButton.setTitle("Pause", forState:.Normal)
            stopButton.enabled = true
            recordWithPermission(false)
            
        }
    }
    
    @IBAction func stop(sender: UIButton) {
        
        showRecordingBtn.hidden = false
        recording_imgVew.image = UIImage (imageLiteral: "rec-dis.png")
        
        stop_Recording_imgVew.image = UIImage (imageLiteral: "rec-list-icon.png")
        
        recordingImageView.image = UIImage (imageLiteral: "rec-big-dis-icon.png")
        
        print("stop")
        
        recorder?.stop()
        player?.stop()
        meterTimer.invalidate()
        
        recordButton.setTitle("Record", forState:.Normal)
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(false)
            stopButton.enabled = false
            recordButton.enabled = true
            statusLabel.text = "00:00"
        } catch let error as NSError {
            print("could not make session inactive")
            print(error.localizedDescription)
        }
    }
    
    @IBAction func play(sender: UIButton) {
        setSessionPlayback()
    }
    
    func setupRecorder() {
        let format = NSDateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        
        let currentFileName = "/recording-\(format.stringFromDate(NSDate())).aiff"
        print(currentFileName)
        
        let libraryPath = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0]
        let soundsPath = libraryPath + "/Sounds"
        let filePath = soundsPath + currentFileName
        
        let fileManager = NSFileManager.defaultManager()
        do {
            
            try fileManager.createDirectoryAtPath(soundsPath, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error1 as NSError {
            print("error" + error1.description)
        }
        
        let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
        
        //ask for permission
        if (audioSession.respondsToSelector(#selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("granted")
                    
                    //set category and activate recorder session
                    try! audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
                    try! audioSession.setActive(true)
                    
                    //create AnyObject of settings
                    if (self.recorder == nil) {
                        let settings: [String : AnyObject] = [
                            AVFormatIDKey:Int(kAudioFormatAppleIMA4), //Int required in Swift2
                            AVSampleRateKey:44100.0,
                            AVNumberOfChannelsKey:2,
                            AVEncoderBitRateKey:12800,
                            AVLinearPCMBitDepthKey:16,
                            AVEncoderAudioQualityKey:AVAudioQuality.Max.rawValue
                        ]
                        
                        //record
                        try! self.recorder = AVAudioRecorder(URL: NSURL.fileURLWithPath(filePath), settings: settings)
                        self.recorder.delegate = self
                    }
                    
                    self.recorder?.record()
                    
                } else{
                    print("not granted")
                }
            })
        }
    }
    
    func recordWithPermission(setup:Bool) {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        // ios 8 and later
        if (session.respondsToSelector(#selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup {
                        self.setupRecorder()
                    }
                    
                    self.recorder.record()
                    self.meterTimer = NSTimer.scheduledTimerWithTimeInterval(0.1,
                        target:self,
                        selector:#selector(VoiceRecoderViewController.updateAudioMeter(_:)),
                        userInfo:nil,
                        repeats:true)
                } else {
                    print("Permission to record not granted")
                }
            })
        } else {
            print("requestRecordPermission unrecognized")
        }
    }
    
    func setSessionPlayback() {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch let error as NSError {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch let error as NSError {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func setSessionPlayAndRecord() {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch let error as NSError {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func deleteAllRecordings() {
        let docsDir =
            NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        do {
            let files = try fileManager.contentsOfDirectoryAtPath(docsDir)
            var recordings = files.filter( { (name: String) -> Bool in
                return name.hasSuffix("aiff")
            })
            for i in 0 ..< recordings.count {
                let path = docsDir + "/" + recordings[i]
                
                print("removing \(path)")
                do {
                    try fileManager.removeItemAtPath(path)
                } catch let error as NSError {
                    NSLog("could not remove \(path)")
                    print(error.localizedDescription)
                }
            }
            
        } catch let error as NSError {
            print("could not get contents of directory at \(docsDir)")
            print(error.localizedDescription)
        }
        
    }
    
    func askForNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector:#selector(VoiceRecoderViewController.background(_:)),
                                                         name:UIApplicationWillResignActiveNotification,
                                                         object:nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector:#selector(VoiceRecoderViewController.foreground(_:)),
                                                         name:UIApplicationWillEnterForegroundNotification,
                                                         object:nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector:#selector(VoiceRecoderViewController.routeChange(_:)),
                                                         name:AVAudioSessionRouteChangeNotification,
                                                         object:nil)
    }
    
    func background(notification:NSNotification) {
        print("background")
    }
    
    func foreground(notification:NSNotification) {
        print("foreground")
    }
    
    
    func routeChange(notification:NSNotification) {
        print("routeChange \(notification.userInfo)")
        
        if let userInfo = notification.userInfo {
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt {
                switch AVAudioSessionRouteChangeReason(rawValue: reason)! {
                case AVAudioSessionRouteChangeReason.NewDeviceAvailable:
                    print("NewDeviceAvailable")
                    print("did you plug in headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.OldDeviceUnavailable:
                    print("OldDeviceUnavailable")
                    print("did you unplug headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.CategoryChange:
                    print("CategoryChange")
                case AVAudioSessionRouteChangeReason.Override:
                    print("Override")
                case AVAudioSessionRouteChangeReason.WakeFromSleep:
                    print("WakeFromSleep")
                case AVAudioSessionRouteChangeReason.Unknown:
                    print("Unknown")
                case AVAudioSessionRouteChangeReason.NoSuitableRouteForCategory:
                    print("NoSuitableRouteForCategory")
                case AVAudioSessionRouteChangeReason.RouteConfigurationChange:
                    print("RouteConfigurationChange")
                    
                }
            }
        }
    }
    
    func checkHeadphones() {
        // check NewDeviceAvailable and OldDeviceUnavailable for them being plugged in/unplugged
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if currentRoute.outputs.count > 0 {
            for description in currentRoute.outputs {
                if description.portType == AVAudioSessionPortHeadphones {
                    print("headphones are plugged in")
                    break
                } else {
                    print("headphones are unplugged")
                }
            }
        } else {
            print("checking headphones requires a connection to a device")
        }
    }
    
    @IBAction
    func trim() {
        if self.soundFileURL == nil {
            print("no sound file")
            return
        }
        
        print("trimming \(soundFileURL!.absoluteString)")
        print("trimming path \(soundFileURL!.lastPathComponent)")
        let asset = AVAsset(URL:self.soundFileURL!)
        exportAsset(asset, fileName: "trimmed.aiff")
    }
    
    func exportAsset(asset:AVAsset, fileName:String) {
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.URLByAppendingPathComponent(fileName)
        print("saving to \(trimmedSoundFileURL.absoluteString)")
        
        if NSFileManager.defaultManager().fileExistsAtPath(trimmedSoundFileURL.absoluteString) {
            print("sound exists, removing \(trimmedSoundFileURL.absoluteString)")
            do {
                var error:NSError?
                if trimmedSoundFileURL.checkResourceIsReachableAndReturnError(&error) {
                    print("is reachable")
                }
                if let e = error {
                    print(e.localizedDescription)
                }
                
                try NSFileManager.defaultManager().removeItemAtPath(trimmedSoundFileURL.absoluteString)
            } catch let error as NSError {
                NSLog("could not remove \(trimmedSoundFileURL)")
                print(error.localizedDescription)
            }
            
        }
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) {
            exporter.outputFileType = AVFileTypeAIFF
            exporter.outputURL = trimmedSoundFileURL
            
            let duration = CMTimeGetSeconds(asset.duration)
            if (duration < 5.0) {
                print("sound is not long enough")
                return
            }
            
            // e.g. the first 5 seconds
            let startTime = CMTimeMake(0, 1)
            let stopTime = CMTimeMake(5, 1)
            exporter.timeRange = CMTimeRangeFromTimeToTime(startTime, stopTime)
            
            // do it
            exporter.exportAsynchronouslyWithCompletionHandler({
                switch exporter.status {
                case  AVAssetExportSessionStatus.Failed:
                    
                    if let e = exporter.error {
                        print("export failed \(e)")
                        switch e.code {
                        case AVError.FileAlreadyExists.rawValue:
                            print("File Exists")
                            break
                        default: break
                        }
                    } else {
                        print("export failed")
                    }
                case AVAssetExportSessionStatus.Cancelled:
                    print("export cancelled \(exporter.error)")
                default:
                    print("export complete")
                }
            })
        }
    }
    
    @IBAction
    func speed() {
        let asset = AVAsset(URL:self.soundFileURL!)
        exportSpeedAsset(asset, fileName: "trimmed.aiff")
    }
    
    func exportSpeedAsset(asset:AVAsset, fileName:String) {
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.URLByAppendingPathComponent(fileName)
        
        let filemanager = NSFileManager.defaultManager()
        if filemanager.fileExistsAtPath(trimmedSoundFileURL.absoluteString) {
            print("sound exists")
        }
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) {
            exporter.outputFileType = AVFileTypeAIFF
            exporter.outputURL = trimmedSoundFileURL
        
            exporter.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmVarispeed
            
            let duration = CMTimeGetSeconds(asset.duration)
            if (duration < 5.0) {
                print("sound is not long enough")
                return
            }
            
            // do it
            exporter.exportAsynchronouslyWithCompletionHandler({
                switch exporter.status {
                case  AVAssetExportSessionStatus.Failed:
                    print("export failed \(exporter.error)")
                case AVAssetExportSessionStatus.Cancelled:
                    print("export cancelled \(exporter.error)")
                default:
                    print("export complete")
                }
            })
        }
    }
}

// MARK: AVAudioRecorderDelegate
extension VoiceRecoderViewController : AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder,
                                         successfully flag: Bool) {
        print("finished recording \(flag)")
        stopButton.enabled = false
        recordButton.setTitle("Record", forState:.Normal)
        
        // iOS8 and later
        let alert = UIAlertController(title: "Recorder",
                                      message: "Finished Recording",
                                      preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Keep", style: .Default, handler: {action in
            print("keep was tapped")
            
        }))
        alert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: {action in
            print("delete was tapped")
            self.recorder.deleteRecording()
        }))
        
        self.presentViewController(alert, animated:true, completion:nil)
        
    }
    
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder,
                                          error: NSError?) {
        
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
}

// MARK: AVAudioPlayerDelegate
extension VoiceRecoderViewController : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        print("finished playing \(flag)")
        recordButton.enabled = true
        stopButton.enabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
}
